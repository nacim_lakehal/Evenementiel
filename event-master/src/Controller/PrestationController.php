<?php

namespace App\Controller;
use Dompdf\Dompdf;
use Dompdf\Options;
use Twig\Environment;
use App\Entity\Employes;
use function Sodium\add;
use App\Entity\Prestation;
use App\Entity\MesPrestation;
use App\Repository\EmplyesRepository;
use App\Repository\EmployesRepository;
use App\Controller\EmployesController ;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class PrestationController extends AbstractController
{
    private $twig;

    public function __construct(Environment $twig)
    {
        $this->twig = $twig;
    }

    /**
     * @Route("/profile/prestation/calendar", name="prestation_calendar", methods={"GET"})
     * @Route("/admin/prestation/calendar", name="prestation_calendar", methods={"GET"})
     */
    public function calendar(): Response
    {
        
        return $this->render('prestation/calendar.html.twig');
    }


     /**
     * @Route("/admin/prestation/{id}/calendarInfo", name="prestation_calendar_info", methods={"GET"})
     */
    public function calendarInfo(): Response
    {
        return new Response($prestation);
    }



    /**
     * @Route("/admin/prestation/new", name="create_prestation")
     * @Route("/admin/prestation/{id}/edit",name="edit_prestation")
     */
    public function create(Request $request ,EntityManagerInterface $manager,prestation $Prestation=null) {
        $modified = true;
        if(!$Prestation){
            $Prestation = new Prestation();
            $modified = false;
        }
        
        $mesprestation = $this->getDoctrine()
            ->getRepository(MesPrestation::class)
            ->findAll();
            $mespres = array();
            foreach ($mesprestation as $m){
                $mespres[$m->getNomPrestation()]=$m->getNomPrestation();
            }
            
        $formPrestation = $this->createFormBuilder($Prestation)
                               ->add('nomClient',TextType::class,[
                                   'attr' => [
                                       'placeholder'=> 'Nom du client'
                                   ]
                               ])
                               ->add('adresse',TextType::class,[
                                'attr' => [
                                    'placeholder'=> 'adresse de la prestation'
                                ]
                            ])
                               ->add('date',DateType::class,array(
                                   'widget' => 'single_text'
                               ))
                               ->add('prestationReserve',ChoiceType::class,
                               ['choices' => $mespres,
                               'multiple'=>true,'expanded'=>true])
                               ->add('total', MoneyType::class, [
                                'currency' => 'DZD'
                            ])

                               -> add('NbInvites',IntegerType::class,['attr' => [
                                'placeholder'=> 'Nombre d\'invités']])

                               ->add('acompte', MoneyType::class, [
                                'currency' => 'DZD'
                            ])
                            ->add('mixte',  ChoiceType::class, [
                                'choices'  => [
                                    'Oui' => true,
                                    'Non' => false
                                    ],
                            ]
                            )
                               ->add('typeTransaction',  ChoiceType::class, [
                                'choices'  => [
                                    'cash' => 'cash',
                                    'CCP' => 'CCP',
                                    'chéque de banque' => 'chéque de banque'],
                            ])
                               ->add('remise', MoneyType::class, [
                                'currency' => 'DZD'
                            ])
                               ->add('frais', MoneyType::class, [
                                'currency' => 'DZD'
                            ])
                
                               ->getForm();

        

        $formPrestation->handleRequest($request);
        if($formPrestation->isSubmitted() && $formPrestation->isValid()){
                          $Prestation->setResteAPayer($Prestation->getTotal()-$Prestation->getAcompte())
                          ->setBenefice($Prestation->getTotal()-$Prestation->getFrais());
             
            $user = $this->getUser();
            if($user->getRoles()[0]=='ROLE_ADMIN' or $user->getRoles()=='ROLE_USER'){
                $Prestation->setStatut('validé');
            }
            else{
                $Prestation->setStatut('En cours');
            } 
            $nbEmployes=0 ;      
            $repo = $this->getDoctrine()->getRepository(MesPrestation::class);     
            for ($i =0;$i<count($Prestation->getPrestationReserve());$i++ ){
                foreach ($mesprestation as $m){
                    if($m->getNomPrestation()==$Prestation->getPrestationReserve()[$i]){
                        $Prestation->addPrestationsReservee($m);
                        $nbEmployes += $m->getNombreEmployes();
                    }
                }
            }
            $Prestation->setNbemployes($nbEmployes)
                       ->setNbEmployesAssignes(0);
            $manager->persist($Prestation);
            $manager->flush();
            return $this->redirectToRoute('show_prestation');

        }
        return $this->render('prestation/new.html.twig',['formPrestation' => $formPrestation->createView(),'modified' => $modified]);
    }
    /**
     *  @Route("/admin/prestation/show", name="show_prestation")
     */
    public function prestation(){
        $repo = $this->getDoctrine()->getRepository(Prestation::class);
        $prestations = $repo->findBy([],['date' => 'DESC']);
        
        //var_dump($prestations[0]);
        return $this->render('prestation/show.html.twig',[
            'controller_name' => 'PrestationController',
            'prestations' => $prestations
        ]);
    }
    /**
     * @Route("/admin/prestation/{id}/delete", name="delete_prestation")
     */
    public function deletePrestation(EntityManagerInterface $manager,Prestation $prestation){
        $manager->remove($prestation);
        $manager->flush();
        return $this->redirectToRoute('show_prestation');
    }

    /**
     * @Route("/admin/prestation/{id}/employes",name="employes_assignes_prestation")
     */
    public function employesAssignes(EmployesRepository $repo,Prestation $prestation,EntityManagerInterface $manager){

       $employesAssignes =  $repo->getEmployesAssigne($prestation);
 

        $employesNonAssignes = $repo->getEmployesNonAssigne($prestation);

    
        
        

        return $this->render('prestation/employesAssignes.html.twig',[

            'prestation' => $prestation,
            'employesNonAssignes' => $employesNonAssignes,
            'employesAssignes' => $employesAssignes

        ]);

    }
       /**
     * @Route("/admin/prestation/assigner",name="prestation_assigner", methods={"POST"})
     */
    public function assignerEmploye(Request $request,EntityManagerInterface $manager ){
        
        //$controller = $this->get('conntrollerEmployes');
        //$rep = $this->forward('App\Controller\EmployesController::employe') ;
        //$employe = $controller->employe();
        $my_array = array();
        $tab = $request->request->get('ide');
        $my_array = json_decode($tab,true);
        $repo = $this->getDoctrine()->getRepository(Employes::class);
        $repo1 = $this->getDoctrine()->getRepository(Prestation::class);
        $prestation = $repo1->find($request->request->get('idp'));

        foreach ($my_array as $value){
            $employe = $repo->find($value);
            $prestation = $prestation->addEmployesAssigne($employe);
            $employe = $employe->addPrestationsAssignee($prestation);
            $manager->flush();
        }
        
        
        return $this->redirectToRoute('prestation_index',["id" => $prestation->getId()]);



    }
    /**
     * @Route("/admin/prestation/retirer",name="prestation_retirer", methods={"POST"})
     */
    public function retirerAssigne(Request $request,EntityManagerInterface $manager){
        $my_array = array();
        $tab = $request->request->get('ide');
        $my_array = json_decode($tab,true);
        $repo = $this->getDoctrine()->getRepository(Employes::class);
        $repo1 = $this->getDoctrine()->getRepository(Prestation::class);
        $prestation = $repo1->find($request->request->get('idp'));
        foreach ($my_array as $value){
        $employe = $repo->find($value);
        $prestation = $prestation->removeEmployesAssigne($employe);
        $employe = $employe->removePrestationsAssignee($prestation);
        $manager->flush();
        }
        return $this->redirectToRoute('prestation_index',["id" => $prestation->getId()]);

    }

    



    /**
     * @Route("/admin/prestation/{id}/details",name="details_prestation")
     */
    public function imprimer(Prestation $prestation,EntityManagerInterface $manager){

         $pdfOptions = new Options();
         $pdfOptions->set('defaultFont', 'Arial');
         
        
         $dompdf = new Dompdf($pdfOptions);
         
         $html = $this->renderView('prestation/details.html.twig',[

            'prestation' => $prestation,
         ]);
         
         
         $dompdf->loadHtml($html);
         
         $dompdf->setPaper('A4', 'portrait');
 
         
         $dompdf->render();
 
         
         $dompdf->stream("recap_prestataion.pdf", [
             "Attachment" => false
         ]);
     }

     /**
      * @Route("/admin/prestation/{id}/index",name="prestation_index")
      */
     public function index(Prestation $prestation){
        $employes = $prestation->getEmployesAssignes();
        return $this->render('prestation/index.html.twig',[

            'employes' => $employes

        ]); 
     }

     /**
      * @Route("/admin/prestation/add",name="ajouter_prestation")
      */
     public function add(EntityManagerInterface $manager){
        $repo = $this->getDoctrine()->getRepository(Employes::class);
        $employe = $repo->find(29);
         $repo1 = $this->getDoctrine()->getRepository(Prestation::class);
         $prestation = $repo1->find(171);
         $prestation = $prestation->addEmployesAssigne($employe);
         $employe = $employe->addPrestationsAssignee($prestation);  
        $manager->flush();
         return $this->redirectToRoute('prestation_index',["id" => 171]);               
     }

     /**
      * @route("admin/prestation/test",name="test")
      */
     public function test(Request $request ,EntityManagerInterface $manager,prestation $Prestation=null){
        $modified = true;
        if(!$Prestation){
            $Prestation = new Prestation();
            $modified = false;
        }
        
        $mesprestation = $this->getDoctrine()
            ->getRepository(MesPrestation::class)
            ->findAll();
            $mespres = array();
            foreach ($mesprestation as $m){
                $mespres[$m->getNomPrestation()]=$m->getNomPrestation();
            }
            
        $formPrestation = $this->createFormBuilder($Prestation)
                               ->add('nomClient',TextType::class,[
                                   'attr' => [
                                       'placeholder'=> 'Nom du client'
                                   ]
                               ])
                               ->add('adresse',TextType::class,[
                                'attr' => [
                                    'placeholder'=> 'adresse de la prestation'
                                ]
                            ])
                               ->add('date',DateType::class,array(
                                   'widget' => 'single_text'
                               ))
                               ->add('prestationReserve',ChoiceType::class,
                               ['choices' => $mespres,
                               'multiple'=>true,'expanded'=>true])
                               ->add('total', MoneyType::class, [
                                'currency' => 'DZD'
                            ])

                               -> add('NbInvites',IntegerType::class,['attr' => [
                                'placeholder'=> 'Nombre d\'invités']])

                               ->add('acompte', MoneyType::class, [
                                'currency' => 'DZD'
                            ])
                            ->add('mixte',  ChoiceType::class, [
                                'choices'  => [
                                    'Oui' => true,
                                    'Non' => false
                                    ],
                            ]
                            )
                               ->add('typeTransaction',  ChoiceType::class, [
                                'choices'  => [
                                    'cash' => 'cash',
                                    'CCP' => 'CCP',
                                    'chéque de banque' => 'chéque de banque'],
                            ])
                               ->add('remise', MoneyType::class, [
                                'currency' => 'DZD'
                            ])
                               ->add('frais', MoneyType::class, [
                                'currency' => 'DZD'
                            ])
                
                               ->getForm();

        

        $formPrestation->handleRequest($request);
        if($formPrestation->isSubmitted() && $formPrestation->isValid()){
                          $Prestation->setResteAPayer($Prestation->getTotal()-$Prestation->getAcompte())
                          ->setBenefice($Prestation->getTotal()-$Prestation->getFrais());
             
            $user = $this->getUser();
            if($user->getRoles()[0]=='ROLE_ADMIN' or $user->getRoles()=='ROLE_USER'){
                $Prestation->setStatut('validé');
            }
            else{
                $Prestation->setStatut('En cours');
            } 
            $nbEmployes=0 ;      
            $repo = $this->getDoctrine()->getRepository(MesPrestation::class);     
            for ($i =0;$i<count($Prestation->getPrestationReserve());$i++ ){
                foreach ($mesprestation as $m){
                    if($m->getNomPrestation()==$Prestation->getPrestationReserve()[$i]){
                        $Prestation->addPrestationsReservee($m);
                        $nbEmployes += $m->getNombreEmployes();
                    }
                }
            }
            $Prestation->setNbemployes($nbEmployes)
                       ->setNbEmployesAssignes(0);
           // $manager->persist($Prestation);
           // $manager->flush();
            return $this->redirectToRoute('show_prestation');

        }
        return $this->render('prestation/test.html.twig',['formPrestation' => $formPrestation->createView(),'modified' => $modified]);



         return $this->render('prestation/test.html.twig');
     }


    }

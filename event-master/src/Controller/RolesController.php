<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class RolesController extends AbstractController
{
    /**
     * @Route("/admin", name="admin_role")
     */
    public function admin()
    {
        return $this->render('roles/admin.html.twig');
    }
    /**
     * @Route("/profile", name="profile_role")
     */
    public function profile()
    {
        return $this->render('roles/profile.html.twig');
    }
}

<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegistrationType ;
use App\Form\ResetPasswordType ;
use App\Form\ForgottelType ;
use Symfony\Component\Form\FormError;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class SecurityController extends AbstractController
{
  /**
  * @Route("/inscription",name="security_registration")
  */  
  public function Registration(Request $request ,EntityManagerInterface $manager, UserPasswordEncoderInterface $encoder) {
      $user = new User();
      $form = $this->createForm(RegistrationType::class,$user);
      $form->handleRequest($request);
      if($form->isSubmitted() && $form->isValid()){
        $user->setRoles(['ROLE_USER']);
        $hash = $encoder->encodePassword($user,$user->getPassword());
        $user->setPassword($hash);
        $manager->persist($user);
        $manager->flush();
        return $this->redirectToRoute('security_login');
      }
      return $this->render('security/registration.html.twig',[
        'form' => $form->createView()]);


  }
  /**
   * @Route("/connexion",name="security_login")
   */

  public function login(){
    return $this->render('security/login.html.twig');
  }
  /**
   * @Route("/deconnexion",name="security_logout")
   */

  public function logout(){}
  
/**
   * @Route("/reset",name="security_reset")
   */

  public function ResetPassword(Request $request,UserPasswordEncoderInterface $encoder)

    {
        
          $em = $this->getDoctrine()->getManager();

            $user = $this->getUser();

          $form = $this->createForm(ResetPasswordType::class, $user);

          $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {

             
                $oldPassword = $request->request->get('reset_password')['oldPassword'] ;
                echo ($oldPassword) ;
                // Si l'ancien mot de passe est bon
    
                if ($encoder->isPasswordValid($user, $oldPassword)) {

                    $newEncodedPassword = $encoder->encodePassword($user, $user->getPlainPassword());

                    $user->setPassword($newEncodedPassword);

                    

                    $em->persist($user);

                    $em->flush();

                    $this->addFlash('notice', 'Votre mot de passe à bien été changé !');

                    return $this->redirectToRoute('index');

                } else {

                    $form->addError(new FormError('Ancien mot de passe incorrect'));

                }

            }

          

    	return $this->render('security/edit.html.twig', array(

    		'form' => $form->createView(),

    	));

    }
    /**
     * @Route("/forgotten",name="forgotten_password")
     */
    public function forgotten(Request $request,UserPasswordEncoderInterface $encoder) {
      $user = new User();
      $em = $this->getDoctrine()->getManager();
      $form = $this->createForm(ForgottelType::class, $user);
      $form->handleRequest($request);
      
      if ($form->isSubmitted() && $form->isValid()) {
        
          $userData = $this->getDoctrine()->getRepository(User::class)->findOneBy(['username' =>$user->getUsernameTmp() ]);
          echo($user->getQuestionTmp()==$userData->getQuestion() && $user->getRepQuestionTmp()==$userData->getRepQuestion());  
          if ($user->getQuestionTmp()==$userData->getQuestion() && $user->getRepQuestionTmp()==$userData->getRepQuestion()){
            
            $newEncodedPassword = $encoder->encodePassword($user, $user->getPlainPassword());

            $userData->setPassword($newEncodedPassword);
                    $em->persist($userData);

                    $em->flush();

                    $this->addFlash('notice', 'Votre mot de passe à bien été changé !');

                    return $this->redirectToRoute('security_login');

                } else {

                    $form->addError(new FormError('votre réponse ne correspond pas '));

                }


          
        
      
      
      
      }
      
      return $this->render('security/forgotten.html.twig', array(

    		'form' => $form->createView(),

      ));
    }
}
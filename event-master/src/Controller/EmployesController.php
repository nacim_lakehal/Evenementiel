<?php

namespace App\Controller;


use App\Entity\Employes;
use App\Entity\Prestation;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class EmployesController extends AbstractController
{
    /**
     * @Route("/admin/employes/new", name="create_employe")
     * @Route("/admin/employes/{id}/edit",name="edit_employe")
     */
    public function create(Request $request ,EntityManagerInterface $manager,Employes $employe=null) {
        $modified = true;
        if(!$employe){
            $employe = new Employes();
            $modified = false;
        }
        
        $formEmploye = $this->createFormBuilder($employe)
                            ->add('Nom',TextType::class,[
                                'attr'=> [
                                    'placeholder' => 'Nom de l\'emplyé ...'
                                ]
                            ])
                            ->add('Prenom',TextType::class,[
                                'attr'=> [
                                    'placeholder' => 'Prénom de l\'emplyé ...'
                                ]
                            ])
                            ->add('typePost',  ChoiceType::class, [
                                'choices'  => [
                                    'Employé polyvalant' => 'Employé polyvalant',
                                    'Gérant' => 'Gérant',
                                    'DJ' => 'DJ'],
                            ])
                            -> add('salaireParPrestation',IntegerType::class,['attr' => [
                                'placeholder'=> 'salaire par prestation ...']])
                            ->getForm();
        
        $formEmploye->handleRequest($request);
        if($formEmploye->isSubmitted() && $formEmploye->isValid()){    
            if(!$modified){
                $employe->setDateDebut(new \DateTime('now'));
            }
            
            $employe->setNbMission(0)
            ->setNbMissionNonPayees(0);
            $manager->persist($employe);
            $manager->flush();
            if ($modified){
                $this->addFlash('success','l\'employes '.$employe->getNom().' a bien été modifié' );
            }else{
                $this->addFlash('success','l\'employes '.$employe->getNom().' a bien été crée' );
            }
            return $this->redirectToRoute('show_employes');
        }
                            return $this->render('Employes/new.html.twig',['formEmploye' => $formEmploye->createView()]);
    }


   
        /**
     *  @Route("/admin/employes", name="show_employes")
     */
    public function employes(){
        $repo = $this->getDoctrine()->getRepository(Employes::class);
        $employes = $repo->findBy([],['NbMission' => 'DESC']);
        return $this->render('employes/index.html.twig',[
            'controller_name' => 'EmployesController',
            'employes' => $employes
        ]);
    }

    /**
     * @Route("/admin/employes/{id}/delete", name="delete_employe", methods="DELETE")
     */
    public function deleteemploye(EntityManagerInterface $manager,Employes $employe){
        $manager->remove($employe);
        $manager->flush();
        $this->addFlash('success','l\'employé '.$employe->getNom().' a bien été supprimé' );
        return $this->redirectToRoute('show_employes');
    }


    public function employe(Request $request){
        return $this->getDoctrine()->getRepository(Employes::class)->find($request->request->get('ide'));
    }
 


    }





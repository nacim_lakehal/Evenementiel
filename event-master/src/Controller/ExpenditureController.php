<?php

namespace App\Controller;
use App\Entity\Expenditure;
use App\Form\ExpenditureType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ExpenditureController extends AbstractController
{
    /**
     * @Route("/admin/expenditure/show", name="show_expenditure")
     */
    public function expenditure(){
        $repo = $this->getDoctrine()->getRepository(Expenditure::class);
        $Expenditures = $repo->findBy([],['date' => 'DESC']);
        return $this->render('Expenditure/index.html.twig',[
            'controller_name' => 'ExpenditureController',
            'Expenditures' => $Expenditures
        ]);
    }
    /**
     * @Route("/admin/expenditure/expenditure", name="expenditure")
     * @Route("/admin/expenditure/{id}/edit",name="edit_expenditure")
     */
    public function add(Request $request ,EntityManagerInterface $manager,Expenditure $Expenditure=null)
    { 
      $modified = true;
        $Expenditure = new Expenditure();
      $form = $this->createForm(ExpenditureType::class,$Expenditure);
      $form->handleRequest($request);
      if($form->isSubmitted() && $form->isValid()){
        $manager->persist($Expenditure);
        $manager->flush();
        return $this->redirectToRoute('show_expenditure');
      }
      return $this->render('expenditure/expenditure.html.twig', [
        'form' => $form->createView(),'modified' => $modified]);

    }


 /**
     * @Route("/profile/expenditure/{id}/delete", name="delete_expenditure")
     */

public function delete(EntityManagerInterface $manager,Expenditure $Expenditure){
  $manager->remove($Expenditure);
  $manager->flush();
  return $this->redirectToRoute('show_expenditure');


}
}
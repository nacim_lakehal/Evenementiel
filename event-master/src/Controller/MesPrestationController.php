<?php

namespace App\Controller;

use Error;
use App\Entity\MesPrestation;
use App\Form\MesPrestationType;
use Doctrine\DBAL\DBALException;
use App\Entity\MesPrestationInterval;
use Doctrine\ORM\EntityManagerInterface;
use phpDocumentor\Reflection\Types\Boolean;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class MesPrestationController extends AbstractController
{
    /**
     * @Route("/admin/mes_prestation/new",name="new_ma_prestation")
     * @Route("/admin/mes_prestation/{id}/edit",name="edit_ma_prestation")
     */
    public function add(Request $request ,EntityManagerInterface $manager,MesPrestation $maPrestation=null)
    { 
        $modified = false;
        if ($maPrestation != null){
            $modified = true;
        }
        else{
        $maPrestation = new MesPrestation();
        }
      $form = $this->createForm(MesPrestationType::class,$maPrestation);
      $form->handleRequest($request);
      if($form->isSubmitted() && $form->isValid()){
          if ($maPrestation->getDependNbInvites() ==true){
            $maPrestation->setDependNbInvites(1);

          }else{
            $maPrestation->setDependNbInvites(0);
          }
          try{
        $manager->persist($maPrestation);
        $manager->flush();
        if($maPrestation->getDependNbInvites()){
            return $this->addInterval($maPrestation);
        }else{
        
        return $this->redirectToRoute('show_mes_prestation');
        }
    }
    catch  (DBALException  $e){
        $this->addFlash('warning','Une erreur c\'est produite, merci de verifier les informations renseignées' );
        return $this->redirectToRoute('new_ma_prestation');
    }
        
    }
    if($modified){

        $this->addFlash('success','Votre modification a bien été enregistée.' );
    }else{
        $this->addFlash('success','Votre prestation a bien été enregistée.' );

    }
      return $this->render('mes_prestation/new.html.twig', [
        'form' => $form->createView(),'modified' => $modified]);
    }

    public function addInterval(MesPrestation $maPrestation ){
        return $this->render('mes_prestation/add_interval.html.twig',[
            'maPrestation' => $maPrestation
        ]);
    }
    /**
     * @Route("/admin/mes_prestation/putAddedInterval",name="putAddedInterval")
     */
    public function putAddedInterval(Request $request,EntityManagerInterface $manager ){
// on recupere notre prestation 

        $repo = $this->getDoctrine()->getRepository(MesPrestation::class);
        $repo1 = $this->getDoctrine()->getRepository(MesPrestationInterval::class);
        $maprestation = $repo->find($request->request->get('idmp'));


        // on recupere les deux tableaux avec les intervalles a ajouter et a supprimer
        $my_array = array();
        $my_array_delete = array();
        $tab = $request->request->get('interval');
        $tabDelete = $request->request->get('supprimer');
        $my_array=json_decode($tab,true);
        $my_array_delete = json_decode($tabDelete,true);
        $mesIntervalActuel = $maprestation->getMesPrestationIntervals();
        if (count($my_array_delete)>0){
            foreach($mesIntervalActuel as $actuelInterval){
                for ($j = 0 ; $j < count($my_array_delete);$j++){
                    if (($my_array_delete[$j][0] == $actuelInterval->getIntervalMin()) ){
                        
                        $maprestation->removeMesPrestationInterval($actuelInterval);
                        
                    }
                }
            }
            try{
            $manager->persist($maprestation);
            $manager->flush();
            } catch  (DBALException $e){
                $this->addFlash('warning','Une erreur c\'est produite, merci de verifier les informations renseignées');
                return $this->redirectToRoute('show_mes_prestation');
            }
        }

        
        
        for ($i = 0 ; $i < count($my_array);$i++){
            $intervalPrestation = new MesPrestationInterval();
            $intervalPrestation->setIntervalMin($my_array[$i][0])
                               ->setIntervalMax($my_array[$i][1])
                               ->setQuantite($my_array[$i][2])
                               ->setQuantiteNonMixte($my_array[$i][3])
                               ->setPrixUnitaire($my_array[$i][4])
                               ->setPrixUnitaireNonMixte($my_array[$i][5]);
            // si c'est la premiere ligne du tableau et que l'intervalle min est à 0
            if($i == 0 && $intervalPrestation->getIntervalMin()==0){
                if($intervalPrestation->getQuantite()==0){
                $intervalPrestation->setTauxInterval(0);

                }else{

                    $intervalPrestation->setTauxInterval($intervalPrestation->getIntervalMax()/$intervalPrestation->getQuantite());
                }
                if($intervalPrestation->getQuantiteNonMixte()==0){

                    $intervalPrestation->setTauxIntervalNonMixte(0);
                }else{
                    $intervalPrestation->setTauxIntervalNonMixte($intervalPrestation->getIntervalMax()/$intervalPrestation->getQuantiteNonMixte());

                }
                $intervalPrestation->setTauxErreur(0)
                             ->setTauxErreurNonMixte(0);





                             
            }
            
            
            // si l'intevall min n'est pas 0 on recupere l'interval précédant pour calculer les taux
            else{
                $tableInterval = $maprestation->getMesPrestationIntervals();
                // on parcours tous les intervals deja inserer pour récuperer le précedant
                foreach($tableInterval as $interval){
                    if ($interval->getIntervalMax() == $intervalPrestation->getIntervalMin()){
                        $intervalPrecedant = $interval;
                    }
                }
                // on verifie que la quantité n'est pas null pour ne pas avoir une division par 0
                // on calcul le taux pour l'interval précédent deja inserer
                if($intervalPrecedant->getQuantite()==0){
                    $tauxmin = 0;
                }else{

                    $tauxmin = ($intervalPrecedant->getIntervalMax())/($intervalPrecedant->getQuantite());
                }
                // on calcul le taux pour l'interval qu'on veux inserer
                if($intervalPrestation->getQuantite()==0){
                    $tauxmax = 0;
                }else{
                    $tauxmax = $intervalPrestation->getIntervalMax()/$intervalPrestation->getQuantite();

                }
                // on insert notre taux
                $intervalPrestation->setTauxInterval(($tauxmin+$tauxmax)/2);
                    // on refait le meme procede si non mixte
                    if($intervalPrecedant->getQuantiteNonMixte()==0){
                        $tauxminNonMixte = 0;
                    }else{
                        $tauxminNonMixte =$intervalPrecedant->getIntervalMax()/$intervalPrecedant->getQuantiteNonMixte();

                    }if($intervalPrestation->getQuantiteNonMixte()==0){
                        $tauxmaxNonMixte = 0;
                    }else{
                        $tauxmaxNonMixte= $intervalPrestation->getIntervalMax()/$intervalPrestation->getQuantiteNonMixte();
                    }

                    // on calcule le taux d'erreur pour mixte et non mixte (tauxErreur = (quantitemax - quantitemin)/2
                    $intervalPrestation->setTauxIntervalNonMixte(($tauxminNonMixte+$tauxmaxNonMixte)/2)
                             ->setTauxErreur(($intervalPrestation->getQuantite()-$intervalPrecedant->getQuantite())/2)
                             ->setTauxErreurNonMixte(($intervalPrestation->getQuantiteNonMixte()-$intervalPrecedant->getQuantiteNonMixte())/2);
            }
            try{
            $maprestation->addMesPrestationInterval($intervalPrestation);
            $manager->persist($intervalPrestation);
            $manager->flush();
            }
            catch  (DBALException $e){
                $this->addFlash('warning','Une erreur c\'est produite, merci de verifier les informations renseignées');
            }
            
        }
        $this->addFlash('success','Votre prestation a bien été enregistée.');
        return $this->redirectToRoute('show_mes_prestation');
    }

    /**
     * @Route("/admin/mes_prestation/show",name="show_mes_prestation")
     */
    public function showMesPrestations(){
        $repo = $this->getDoctrine()->getRepository(MesPrestation::class);
        $mesprestation = $repo->findAll();
        return $this->render('mes_prestation/show.html.twig',[
            'mesprestation' => $mesprestation
        ]);
    }
    /**
     * @Route("/admin/mes_prestation/{id}/delete",name="delete_mes_prestation")
     */
    public function deleteMaPrestation(EntityManagerInterface $manager,MesPrestation $maprestation){
        //try{
            $manager->remove($maprestation);
            $manager->flush();
         //   $this->addFlash('success','Votre prestation a bien été suprimée.');
        //} catch  (DBALException $e){
          //  $this->addFlash('warning','Une erreur c\'est produite, impossible de supprimer cette prestation');
       // }
        return $this->redirectToRoute('show_mes_prestation');
    }


}

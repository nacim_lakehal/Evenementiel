<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200516223942 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE prestation_mes_prestation (prestation_id INT NOT NULL, mes_prestation_id INT NOT NULL, INDEX IDX_2078B9C59E45C554 (prestation_id), INDEX IDX_2078B9C51C231958 (mes_prestation_id), PRIMARY KEY(prestation_id, mes_prestation_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE prestation_mes_prestation ADD CONSTRAINT FK_2078B9C59E45C554 FOREIGN KEY (prestation_id) REFERENCES prestation (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE prestation_mes_prestation ADD CONSTRAINT FK_2078B9C51C231958 FOREIGN KEY (mes_prestation_id) REFERENCES mes_prestation (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE prestation ADD statut VARCHAR(255) NOT NULL, ADD mixte TINYINT(1) NOT NULL, DROP prestation_reserve, CHANGE date_end date_end DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE user CHANGE roles roles JSON NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE prestation_mes_prestation');
        $this->addSql('ALTER TABLE prestation ADD prestation_reserve JSON CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_bin` COMMENT \'(DC2Type:json_array)\', DROP statut, DROP mixte, CHANGE date_end date_end DATETIME DEFAULT \'NULL\'');
        $this->addSql('ALTER TABLE user CHANGE roles roles LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_bin`');
    }
}

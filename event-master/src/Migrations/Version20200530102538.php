<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200530102538 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE mes_prestation_interval (id INT AUTO_INCREMENT NOT NULL, ma_prestation_id INT DEFAULT NULL, interval_min INT NOT NULL, interval_max INT NOT NULL, quantite INT NOT NULL, prix_unitaire INT NOT NULL, taux_interval INT NOT NULL, taux_erreur INT NOT NULL, INDEX IDX_57ED67CAC7DBE63D (ma_prestation_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE mes_prestation_interval ADD CONSTRAINT FK_57ED67CAC7DBE63D FOREIGN KEY (ma_prestation_id) REFERENCES mes_prestation (id)');
        $this->addSql('ALTER TABLE mes_prestation DROP interval_min, DROP interval_max, DROP taux_interval, DROP taux_erreur, DROP prix_unitaire');
        $this->addSql('ALTER TABLE prestation CHANGE date_end date_end DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE user CHANGE roles roles JSON NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE mes_prestation_interval');
        $this->addSql('ALTER TABLE mes_prestation ADD interval_min INT DEFAULT NULL, ADD interval_max INT DEFAULT NULL, ADD taux_interval INT DEFAULT NULL, ADD taux_erreur INT DEFAULT NULL, ADD prix_unitaire INT DEFAULT NULL');
        $this->addSql('ALTER TABLE prestation CHANGE date_end date_end DATETIME DEFAULT \'NULL\'');
        $this->addSql('ALTER TABLE user CHANGE roles roles LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_bin`');
    }
}

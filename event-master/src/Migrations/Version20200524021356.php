<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200524021356 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE mes_prestation ADD depend_nb_invites TINYINT(1) NOT NULL, ADD interval_min INT DEFAULT NULL, ADD interval_max INT DEFAULT NULL, ADD taux_interval INT DEFAULT NULL, ADD taux_erreur INT DEFAULT NULL, ADD prix_unitaire INT DEFAULT NULL');
        $this->addSql('ALTER TABLE prestation CHANGE date_end date_end DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE user CHANGE roles roles JSON NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE mes_prestation DROP depend_nb_invites, DROP interval_min, DROP interval_max, DROP taux_interval, DROP taux_erreur, DROP prix_unitaire');
        $this->addSql('ALTER TABLE prestation CHANGE date_end date_end DATETIME DEFAULT \'NULL\'');
        $this->addSql('ALTER TABLE user CHANGE roles roles LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_bin`');
    }
}

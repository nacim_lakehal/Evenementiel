<?php

namespace App\EventSubscriber;

use CalendarBundle\Entity\Event;
use CalendarBundle\CalendarEvents;
use App\Repository\PrestationRepository;
use CalendarBundle\Event\CalendarEvent;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class CalendarSubscriber implements EventSubscriberInterface
{
    private $PrestationRepository;
    private $router;

    public function __construct(
        PrestationRepository $PrestationRepository,
        UrlGeneratorInterface $router
    ) {
        $this->PrestationRepository = $PrestationRepository;
        $this->router = $router;
    }

    public static function getSubscribedEvents()
    {
        return [
            CalendarEvents::SET_DATA => 'onCalendarSetData',
        ];
    }

    public function onCalendarSetData(CalendarEvent $calendar)
    {
        $start = $calendar->getStart();
        $end = $calendar->getEnd();
        $filters = $calendar->getFilters();
        var_dump('onCalendarSetData');
        // Modify the query to fit to your entity and needs
        // Change booking.beginAt by your start date property
        $prestations = $this->PrestationRepository
            ->createQueryBuilder('prestation')
            ->where('prestation.date BETWEEN :start and :end OR prestation.dateEnd BETWEEN :start and :end')
            ->setParameter('start', $start->format('Y-m-d H:i:s'))
            ->setParameter('end', $end->format('Y-m-d H:i:s'))
            ->getQuery()
            ->getResult()
        ;

        foreach ($prestations as $prestation) {
            // this create the events with your data (here booking data) to fill calendar
            $prestationEvent = new Event(
                $prestation->getnomClient(),
                $prestation->getDate(),
                $prestation->getDateEnd()// If the end date is null or not defined, a all day event is created.
            );
            var_dump('New event');

            /*
             * Add custom options to events
             *
             * For more information see: https://fullcalendar.io/docs/event-object
             * and: https://github.com/fullcalendar/fullcalendar/blob/master/src/core/options.ts
             */
            setlocale(LC_TIME, 'fr_FR.utf8','fra');
            $dateFr = strftime("%A %d %B %Y",$prestation->getDate()->getTimeStamp());
            $heureDebut = date_format($prestation->getDate(),'H:m');
            if($prestation->getDateEnd()!=null){
                $heureFin = date_format($prestation->getDateEnd(),'H:m');
            }else{
                $heureFin = null;
            }
            // on recupere toutes les informations de la prestation
            $detailPrestation = [];
            $Mes = $prestation->getPrestationsReservees();
            foreach ($Mes as $mapres){
                $detailPrestation[] = [$mapres->getId(),$mapres->getNomPrestation(),$mapres->getNombreEmployes(),number_format($mapres->getPrix(),2,'.',' ')];
            }

            if($prestation->getMixte()==true){
                $mixte = 'Oui';
            }else{
                $mixte = 'Non';
            }
           
            
            
            $prestationEvent->setOptions([
                'backgroundColor' => '#E96673',
                'borderColor' => '#E96673',
                'description' =>[
                'id' => $prestation->getId(),
                'NbInvites'=>$prestation->getNbInvites(),
                'adresse' => $prestation->getAdresse(),
                'nomClient' => $prestation->getNomClient(),
                'nbEmployes' => $prestation->getNbEmployes(),
                'nbEmployesAssignes' => $prestation->getNbEmployesAssignes(),
                'date' => $dateFr,
                'heureDebut'=> $heureDebut,
                'heureFin' =>$heureFin,
                'prestationReserve' =>$detailPrestation,
                'total' => number_format($prestation->getTotal(),2,'.',' '),
                'accomptes'=>number_format($prestation->getAcompte(),2,'.',' '),
                'remises'=>number_format($prestation->getRemise(),2,'.',' '),
                'resteAPayer'=>number_format($prestation->getResteAPayer(),2,'.',' '),
                // a modifier
                'statut' => $prestation->getStatut(),
                'mixte' => $mixte
                ]                
            ]);
            $prestationEvent->addOption(
                'url',$this->router->generate('prestation_calendar_info', [
                    'id' => $prestation->getId(),
                ]));
            // finally, add the event to the CalendarEvent to fill the calendar
            $calendar->addEvent($prestationEvent);
        }
    }
}
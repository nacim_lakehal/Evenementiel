<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EmployesRepository")
 */
class Employes
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Nom;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Prenom;

    /**
     * @ORM\Column(type="date")
     */
    private $DateDebut;

    /**
     * @ORM\Column(type="integer")
     */
    private $NbMission;

    /**
     * @ORM\Column(type="integer")
     */
    private $NbMissionNonPayees;

    /**
     * @ORM\Column(type="integer")
     */
    private $SalaireParPrestation;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $TypePost;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Prestation", mappedBy="EmployesAssignes")
     */
    private $PrestationsAssignees;

    public function __construct()
    {
        $this->PrestationsAssignees = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->Nom;
    }

    public function setNom(string $Nom): self
    {
        $this->Nom = $Nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->Prenom;
    }

    public function setPrenom(string $Prenom): self
    {
        $this->Prenom = $Prenom;

        return $this;
    }

    public function getDateDebut(): ?\DateTimeInterface
    {
        return $this->DateDebut;
    }

    public function setDateDebut(\DateTimeInterface $DateDebut): self
    {
        $this->DateDebut = $DateDebut;

        return $this;
    }

    public function getNbMission(): ?int
    {
        return $this->NbMission;
    }

    public function setNbMission(int $NbMission): self
    {
        $this->NbMission = $NbMission;

        return $this;
    }

    public function getNbMissionNonPayees(): ?int
    {
        return $this->NbMissionNonPayees;
    }

    public function setNbMissionNonPayees(int $NbMissionNonPayees): self
    {
        $this->NbMissionNonPayees = $NbMissionNonPayees;

        return $this;
    }

    public function getSalaireParPrestation(): ?int
    {
        return $this->SalaireParPrestation;
    }

    public function setSalaireParPrestation(int $SalaireParPrestation): self
    {
        $this->SalaireParPrestation = $SalaireParPrestation;

        return $this;
    }

    public function getTypePost(): ?string
    {
        return $this->TypePost;
    }

    public function setTypePost(string $TypePost): self
    {
        $this->TypePost = $TypePost;

        return $this;
    }

    /**
     * @return Collection|Prestation[]
     */
    public function getPrestationsAssignees(): Collection
    {
        return $this->PrestationsAssignees;
    }

    public function addPrestationsAssignee(Prestation $prestationsAssignee): self
    {
        if (!$this->PrestationsAssignees->contains($prestationsAssignee)) {
            $this->PrestationsAssignees[] = $prestationsAssignee;
            $prestationsAssignee->addEmployesAssigne($this);
        }

        return $this;
    }

    public function removePrestationsAssignee(Prestation $prestationsAssignee): self
    {
        if ($this->PrestationsAssignees->contains($prestationsAssignee)) {
            $this->PrestationsAssignees->removeElement($prestationsAssignee);
            $prestationsAssignee->removeEmployesAssigne($this);
        }

        return $this;
    }
}

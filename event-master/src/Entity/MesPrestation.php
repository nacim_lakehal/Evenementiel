<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MesPrestationRepository")
 */
class MesPrestation
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $nomPrestation;

    /**
     * @ORM\Column(type="integer")
     */
    private $nombreEmployes;

    /**
     * @ORM\Column(type="integer")
     */
    private $prix;

    /**
     * @ORM\ManyToMany(targetEntity=Prestation::class, mappedBy="PrestationsReservees")
     */
    private $prestations;

    /**
     * @ORM\Column(type="boolean")
     */
    private $DependNbInvites;

    /**
     * @ORM\OneToMany(targetEntity=MesPrestationInterval::class, mappedBy="MaPrestation", cascade={"remove"})
     */
    private $mesPrestationIntervals;

   

    public function __construct()
    {
        $this->prestations = new ArrayCollection();
        $this->mesPrestationIntervals = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomPrestation(): ?string
    {
        return $this->nomPrestation;
    }

    public function setNomPrestation(string $nomPrestation): self
    {
        $this->nomPrestation = $nomPrestation;

        return $this;
    }

    public function getNombreEmployes(): ?int
    
    {
        return $this->nombreEmployes;
    }

    public function setNombreEmployes(int $nombreEmployes): self
    {
        $this->nombreEmployes = $nombreEmployes;

        return $this;
    }

    public function getPrix(): ?int
    {
        return $this->prix;
    }

    public function setPrix(int $prix): self
    {
        $this->prix = $prix;

        return $this;
    }

    /**
     * @return Collection|Prestation[]
     */
    public function getPrestations(): Collection
    {
        return $this->prestations;
    }

    public function addPrestation(Prestation $prestation): self
    {
        if (!$this->prestations->contains($prestation)) {
            $this->prestations[] = $prestation;
            $prestation->addPrestationsReservee($this);
        }

        return $this;
    }

    public function removePrestation(Prestation $prestation): self
    {
        if ($this->prestations->contains($prestation)) {
            $this->prestations->removeElement($prestation);
            $prestation->removePrestationsReservee($this);
        }

        return $this;
    }

    public function getDependNbInvites(): ?bool
    {
        return $this->DependNbInvites;
    }

    public function setDependNbInvites(bool $DependNbInvites): self
    {
        $this->DependNbInvites = $DependNbInvites;

        return $this;
    }

    /**
     * @return Collection|MesPrestationInterval[]
     */
    public function getMesPrestationIntervals(): Collection
    {
        return $this->mesPrestationIntervals;
    }

    public function addMesPrestationInterval(MesPrestationInterval $mesPrestationInterval): self
    {
        if (!$this->mesPrestationIntervals->contains($mesPrestationInterval)) {
            $this->mesPrestationIntervals[] = $mesPrestationInterval;
            $mesPrestationInterval->setMaPrestation($this);
        }

        return $this;
    }

    public function removeMesPrestationInterval(MesPrestationInterval $mesPrestationInterval): self
    {
        if ($this->mesPrestationIntervals->contains($mesPrestationInterval)) {
            $this->mesPrestationIntervals->removeElement($mesPrestationInterval);
            // set the owning side to null (unless already changed)
            if ($mesPrestationInterval->getMaPrestation() === $this) {
                $mesPrestationInterval->setMaPrestation(null);
            }
        }

        return $this;
    }
}

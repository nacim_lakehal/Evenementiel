<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PrestationRepository")
 */
class Prestation
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nomClient;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $adresse;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateEnd;
   
    /**
     * @ORM\Column(type="integer")
     */
    private $Total;

    /**
     * @ORM\Column(type="integer")
     */
    private $acompte;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $typeTransaction;

    /**
     * @ORM\Column(type="integer")
     */
    private $resteAPayer;

    /**
     * @ORM\Column(type="integer")
     */
    private $Remise;

    /**
     * @ORM\Column(type="integer")
     */
    private $frais;

    /**
     * @ORM\Column(type="integer")
     */
    private $benefice;

    /**
     * @ORM\Column(type="integer")
     */
    private $NbInvites;
    /**
     * @ORM\Column(type="integer")
     */
    private $NbEmployes ;

    /**
     * @ORM\Column(type="integer")
     */
    private $NbEmployesAssignes;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Employes", inversedBy="PrestationsAssignees")
     * @ORM\JoinTable(name="prestation_employes")
     */
    private $EmployesAssignes;

    /**
     * @ORM\ManyToMany(targetEntity=MesPrestation::class, inversedBy="prestations")
     */
    private $PrestationsReservees;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Statut;



    private $prestationReserve = [];

    /**
     * @ORM\Column(type="boolean")
     */
    private $Mixte; 
    

    public function __construct()
    {
        $this->EmployesAssignes = new ArrayCollection();
        $this->PrestationsReservees = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomClient(): ?string
    {
        return $this->nomClient;
    }

    public function setNomClient(string $nomClient): self
    {
        $this->nomClient = $nomClient;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }



    public function getTotal(): ?int
    {
        return $this->Total;
    }

    public function setTotal(int $Total): self
    {
        $this->Total = $Total;

        return $this;
    }

    public function getAcompte(): ?int
    {
        return $this->acompte;
    }

    public function setAcompte(int $acompte): self
    {
        $this->acompte = $acompte;

        return $this;
    }

    public function getTypeTransaction(): ?string
    {
        return $this->typeTransaction;
    }

    public function setTypeTransaction(string $typeTransaction): self
    {
        $this->typeTransaction = $typeTransaction;

        return $this;
    }

    public function getResteAPayer(): ?int
    {
        return $this->resteAPayer;
    }

    public function setResteAPayer(int $resteAPayer): self
    {
        $this->resteAPayer = $resteAPayer;

        return $this;
    }

    public function getRemise(): ?int
    {
        return $this->Remise;
    }

    public function setRemise(int $Remise): self
    {
        $this->Remise = $Remise;

        return $this;
    }

    public function getFrais(): ?int
    {
        return $this->frais;
    }

    public function setFrais(int $frais): self
    {
        $this->frais = $frais;

        return $this;
    }

    public function getBenefice(): ?int
    {
        return $this->benefice;
    }

    public function setBenefice(int $benefice): self
    {
        $this->benefice = $benefice;

        return $this;
    }

    public function getNbInvites(): ?int
    {
        return $this->NbInvites;
    }

    public function setNbInvites(int $NbInvites): self
    {
        $this->NbInvites = $NbInvites;

        return $this;
    }
    public function getNbEmployes(): ?int
    {
        return $this->NbEmployes;
    }

    public function setNbEmployes(int $NbEmployes): self
    {
        $this->NbEmployes = $NbEmployes;

        return $this;
    }

    public function getNbEmployesAssignes(): ?int
    {
        return $this->NbEmployesAssignes;
    }

    public function setNbEmployesAssignes(int $NbEmployesAssignes): self
    {
        $this->NbEmployesAssignes = $NbEmployesAssignes;

        return $this;
    }

    /**
     * @return Collection|Employes[]
     */
    public function getEmployesAssignes(): Collection
    {
        return $this->EmployesAssignes;
    }

    public function addEmployesAssigne(Employes $employesAssigne): self
    {
        if (!$this->EmployesAssignes->contains($employesAssigne)) {
            $this->EmployesAssignes[] = $employesAssigne;
        }

        return $this;
    }

    public function removeEmployesAssigne(Employes $employesAssigne): self
    {
        if ($this->EmployesAssignes->contains($employesAssigne)) {
            $this->EmployesAssignes->removeElement($employesAssigne);
        }

        return $this;
    }

    public function getDateEnd(): ?\DateTimeInterface
    {
        return $this->dateEnd;
    }

    public function setDateEnd(?\DateTimeInterface $dateEnd): self
    {
        $this->dateEnd = $dateEnd;

        return $this;
    }

    /**
     * @return Collection|MesPrestation[]
     */
    public function getPrestationsReservees(): Collection
    {
        return $this->PrestationsReservees;
    }

    public function addPrestationsReservee(MesPrestation $prestationsReservee): self
    {
        if (!$this->PrestationsReservees->contains($prestationsReservee)) {
            $this->PrestationsReservees[] = $prestationsReservee;
        }

        return $this;
    }

    public function removePrestationsReservee(MesPrestation $prestationsReservee): self
    {
        if ($this->PrestationsReservees->contains($prestationsReservee)) {
            $this->PrestationsReservees->removeElement($prestationsReservee);
        }

        return $this;
    }

    public function getStatut(): ?string
    {
        return $this->Statut;
    }

    public function setStatut(string $Statut): self
    {
        $this->Statut = $Statut;

        return $this;
    }

    public function getPrestationReserve(): ?array
    {
        return $this->prestationReserve;
    }

    public function setPrestationReserve(array $prestationReserve): self
    {
        $this->prestationReserve = $prestationReserve;

        return $this;
    }

    public function getMixte(): ?bool
    {
        return $this->Mixte;
    }

    public function setMixte(bool $Mixte): self
    {
        $this->Mixte = $Mixte;

        return $this;
    }


    
}

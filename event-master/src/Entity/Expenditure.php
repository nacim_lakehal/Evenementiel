<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ExpenditureRepository")
 */
class Expenditure
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ExName;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ExCategory;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date;

    /**
     * @ORM\Column(type="integer")
     */
    private $Price;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getExName(): ?string
    {
        return $this->ExName;
    }

    public function setExName(string $ExName): self
    {
        $this->ExName = $ExName;

        return $this;
    }

    public function getExCategory(): ?string
    {
        return $this->ExCategory;
    }

    public function setExCategory(string $ExCategory): self
    {
        $this->ExCategory = $ExCategory;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getPrice(): ?int
    {
        return $this->Price;
    }

    public function setPrice(int $Price): self
    {
        $this->Price = $Price;

        return $this;
    }
}

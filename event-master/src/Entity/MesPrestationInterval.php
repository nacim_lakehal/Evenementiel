<?php

namespace App\Entity;

use App\Repository\MesPrestationIntervalRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=MesPrestationIntervalRepository::class)
 */
class MesPrestationInterval
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $IntervalMin;

    /**
     * @ORM\Column(type="integer")
     */
    private $IntervalMax;

    /**
     * @ORM\Column(type="integer")
     */
    private $quantite;

    /**
     * @ORM\Column(type="integer")
     */
    private $prixUnitaire;

    /**
     * @ORM\Column(type="integer")
     */
    private $tauxInterval;

    /**
     * @ORM\Column(type="integer")
     */
    private $tauxErreur;

    /**
     * @ORM\ManyToOne(targetEntity=MesPrestation::class, inversedBy="mesPrestationIntervals",cascade={"remove"})
     */
    private $MaPrestation;

    /**
     * @ORM\Column(type="integer")
     */
    private $quantiteNonMixte;

    /**
     * @ORM\Column(type="integer")
     */
    private $prixUnitaireNonMixte;

    /**
     * @ORM\Column(type="integer")
     */
    private $tauxIntervalNonMixte;

    /**
     * @ORM\Column(type="integer")
     */
    private $tauxErreurNonMixte;

   

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIntervalMin(): ?int
    {
        return $this->IntervalMin;
    }

    public function setIntervalMin(int $IntervalMin): self
    {
        $this->IntervalMin = $IntervalMin;

        return $this;
    }

    public function getIntervalMax(): ?int
    {
        return $this->IntervalMax;
    }

    public function setIntervalMax(int $IntervalMax): self
    {
        $this->IntervalMax = $IntervalMax;

        return $this;
    }

    public function getQuantite(): ?int
    {
        return $this->quantite;
    }

    public function setQuantite(int $quantite): self
    {
        $this->quantite = $quantite;

        return $this;
    }

    public function getPrixUnitaire(): ?int
    {
        return $this->prixUnitaire;
    }

    public function setPrixUnitaire(int $prixUnitaire): self
    {
        $this->prixUnitaire = $prixUnitaire;

        return $this;
    }

    public function getTauxInterval(): ?int
    {
        return $this->tauxInterval;
    }

    public function setTauxInterval(int $tauxInterval): self
    {
        $this->tauxInterval = $tauxInterval;

        return $this;
    }

    public function getTauxErreur(): ?int
    {
        return $this->tauxErreur;
    }

    public function setTauxErreur(int $tauxErreur): self
    {
        $this->tauxErreur = $tauxErreur;

        return $this;
    }

    public function getMaPrestation(): ?MesPrestation
    {
        return $this->MaPrestation;
    }

    public function setMaPrestation(?MesPrestation $MaPrestation): self
    {
        $this->MaPrestation = $MaPrestation;

        return $this;
    }

    public function getQuantiteNonMixte(): ?int
    {
        return $this->quantiteNonMixte;
    }

    public function setQuantiteNonMixte(int $quantiteNonMixte): self
    {
        $this->quantiteNonMixte = $quantiteNonMixte;

        return $this;
    }

    public function getPrixUnitaireNonMixte(): ?int
    {
        return $this->prixUnitaireNonMixte;
    }

    public function setPrixUnitaireNonMixte(int $prixUnitaireNonMixte): self
    {
        $this->prixUnitaireNonMixte = $prixUnitaireNonMixte;

        return $this;
    }

    public function getTauxIntervalNonMixte(): ?int
    {
        return $this->tauxIntervalNonMixte;
    }

    public function setTauxIntervalNonMixte(int $tauxIntervalNonMixte): self
    {
        $this->tauxIntervalNonMixte = $tauxIntervalNonMixte;

        return $this;
    }

    public function getTauxErreurNonMixte(): ?int
    {
        return $this->tauxErreurNonMixte;
    }

    public function setTauxErreurNonMixte(int $tauxErreurNonMixte): self
    {
        $this->tauxErreurNonMixte = $tauxErreurNonMixte;

        return $this;
    }

}

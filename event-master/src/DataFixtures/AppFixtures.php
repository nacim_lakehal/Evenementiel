<?php

namespace App\DataFixtures;

use Faker\Factory;
use App\Entity\User;
use App\Entity\Employes;
use App\Entity\Prestation;
use App\Entity\Expenditure;
use App\Entity\MesPrestation;
use App\DataFixtures\AppFixtures;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


class AppFixtures extends Fixture 
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder){
        $this->encoder = $encoder;
    }




    public function load(ObjectManager $manager){
        $array1 = array ('cash','CCP','chéque de banque');
        $faker = Factory::create('fr_FR');
        $maPrestation = new MesPrestation();
        $maPrestation->setNomPrestation('Animateur DJ')
                     ->setNombreEmployes(2)
                     ->setPrix(35000);
        $manager->persist($maPrestation);
        $manager->flush();
        $maPrestation = new MesPrestation();
        $maPrestation->setNomPrestation('videographe')
                     ->setNombreEmployes(2)
                     ->setPrix(40000);
        $manager->persist($maPrestation);
        $manager->flush();
        $manager->persist($maPrestation);
        $manager->flush();
        $maPrestation = new MesPrestation();
        $maPrestation->setNomPrestation('photographe')
                     ->setNombreEmployes(1)
                     ->setPrix(30000);
        $manager->persist($maPrestation);
        $manager->flush();
        
        $repo = $manager->getRepository(MesPrestation::class);
        $mespres = $repo->getAllId();
        $mesprestation = $repo->findAll();




        for ($i = 0 ; $i < 20 ; $i++){
            $randIndexprestation = array_rand($mesprestation);
            $randIndex = array_rand($array1);
            $prestation = new Prestation();
            $prestation->setNomClient($faker->firstName())
                       ->setAdresse($faker->address())
                       ->setDate($faker->dateTimeInInterval($startDate = '-30 days', $interval = '+ 50 days'))
                       ->setPrestationReserve($faker->randomElements(array_values($mespres),$count = 3))
                       ->setTotal($faker->numberBetween($min = 10000, $max = 100000))
                       ->setAcompte($faker->numberBetween($min = 1000, $max = 10000))
                       ->setTypeTransaction($array1[$randIndex])
                       ->setResteAPayer($prestation->getTotal()-$prestation->getAcompte())
                       ->setRemise($faker->numberBetween($min = 1000, $max = 10000))
                       ->setFrais($faker->numberBetween($min = 1000, $max = 10000))
                       ->setBenefice($prestation->getTotal()-$prestation->getFrais()-$prestation->getRemise())
                       ->setNbEmployes($faker->numberBetween($min = 1, $max = 15))
                       ->setNbInvites($faker->numberBetween($min = 1, $max = 2000))
                       ->setNbEmployesAssignes(0)
                       ->setMixte(true)
                       ->setStatut('Validé')
                       ;
            for ($j=0 ;$j <= $randIndexprestation ;$j++){
                $prestation->addPrestationsReservee($mesprestation[$j]);
            }
                       
            $manager->persist($prestation);
            $manager->flush();
        }
        $array2 = array ('fourniture','son&lumiere','animation','cuisine');
        $array3 = array ('chaises','tables','drone','plats','baffles','projecteurs','tapis rouge');

        for ($i = 0 ; $i < 20 ; $i++){
            $randIndex2 = array_rand($array2);
            $randIndex3= array_rand($array3);
            $expenditure = new Expenditure();
            $expenditure->setExName($array3[$randIndex3])
                        ->setExCategory($array2[$randIndex2])
                        ->setDate($faker->dateTime())
                        ->setPrice($faker->numberBetween($min = 1000, $max = 15000));

            $manager->persist($expenditure);
            $manager->flush();
    }
    $array4 = array ('Polivalant','DJ','décorateur','cuisinier','gerant','photographe');
    for ($i = 0 ; $i < 10 ; $i++){
        $randIndex4 = array_rand($array4);
       $employe = new Employes();
        $employe->setNom($faker->firstname())
                    ->setPrenom($faker->lastName())
                    ->setDateDebut($faker->dateTime())
                    ->setNbMission($faker->numberBetween($min = 0, $max = 30))
                    ->setSalaireParPrestation($faker->numberBetween($min = 1000, $max = 10000))
                    ->setNbMissionNonPayees($faker->numberBetween($min = 0, $max = 30))
                    ->setTypePost($array4[$randIndex4])
                    ;

        $manager->persist($employe);
        $manager->flush();
}



    $user = new User();
    $password = 'adminadmin';
    $hash = $this->encoder->encodePassword($user,$password);
    $user->setName('admin')
         ->setUsername('admin')
         ->setRoles(["ROLE_ADMIN"])
         ->setPassword($hash)
         ->setFirstName('admin')
         ->setPhoneNumber('010101')
         ->setQuestion('Quelle est ma couleur préférée ?')
         ->setRepQuestion('admin');
         $manager->persist($user);
         $manager->flush();
}
}

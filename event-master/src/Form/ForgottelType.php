<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;

class ForgottelType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('usernameTmp')
        ->add('questionTmp',ChoiceType::class,[
            'choices'  => [
                'Quelle est ma couleur préférée ?' => 'Quelle est ma couleur préférée ?',
                'quelle est votre ville favorite' => 'quelle est votre ville favorite',
                'quelle est votre équipe sportive favorite' => 'quelle est votre équipe sportive favorite',
                'Quelle était le nom de votre école primaire' => 'Quelle était le nom de votre école primaire',
                'Quel est le nom de jeune fille de votre mére' => 'Quel est le nom de jeune fille de votre mére'
            ],
        ])
        ->add('repQuestionTmp')
        ->add('plainPassword', RepeatedType::class, array(

            'type' => PasswordType::class,

            'invalid_message' => 'Les deux mots de passe doivent être identiques',

            'options' => array(

                'attr' => array(

                    'class' => 'password-field'

                )

            )))
        ;

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}

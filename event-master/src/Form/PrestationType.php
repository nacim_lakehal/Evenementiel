<?php

namespace App\Form;

use App\Entity\Prestation;
use App\Entity\MesPrestation;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Doctrine\ORM\EntityManagerInterface;


class PrestationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        $builder
        ->add('nomClient',TextType::class,[
            'attr' => [
                'placeholder'=> 'Nom du client'
            ]
        ])
        ->add('adresse',TextType::class,[
         'attr' => [
             'placeholder'=> 'adresse de la prestation'
         ]
     ])
        ->add('date',DateType::class,array(
            'widget' => 'single_text'
        ))
        ->add('prestationReserve',ChoiceType::class,
        ['choices' => $mespres,
        'multiple'=>true,'expanded'=>true])
        ->add('total', MoneyType::class, [
         'currency' => 'DZD'
     ])

        -> add('NbInvites',IntegerType::class,['attr' => [
         'placeholder'=> 'Nombre d\'invités']])

        ->add('acompte', MoneyType::class, [
         'currency' => 'DZD'
     ])
        ->add('typeTransaction',  ChoiceType::class, [
         'choices'  => [
             'cash' => 'cash',
             'CCP' => 'CCP',
             'chéque de banque' => 'chéque de banque'],
     ])
        ->add('remise', MoneyType::class, [
         'currency' => 'DZD'
     ])
        ->add('frais', MoneyType::class, [
         'currency' => 'DZD'
     ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Prestation::class,
        ]);
    }
}

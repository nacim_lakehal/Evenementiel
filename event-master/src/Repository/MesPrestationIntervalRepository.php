<?php

namespace App\Repository;

use App\Entity\MesPrestationInterval;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method MesPrestationInterval|null find($id, $lockMode = null, $lockVersion = null)
 * @method MesPrestationInterval|null findOneBy(array $criteria, array $orderBy = null)
 * @method MesPrestationInterval[]    findAll()
 * @method MesPrestationInterval[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MesPrestationIntervalRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MesPrestationInterval::class);
    }

    // /**
    //  * @return MesPrestationInterval[] Returns an array of MesPrestationInterval objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MesPrestationInterval
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

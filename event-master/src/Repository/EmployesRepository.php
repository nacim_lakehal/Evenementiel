<?php

namespace App\Repository;
use Doctrine\DBAL\Query\QueryBuilder;
use App\Entity\Employes;
use App\Controller\EmployesController ;
use App\Entity\Prestation;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method Employes|null find($id, $lockMode = null, $lockVersion = null)
 * @method Employes|null findOneBy(array $criteria, array $orderBy = null)
 * @method Employes[]    findAll()
 * @method Employes[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EmployesRepository extends ServiceEntityRepository
{

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Employes::class);
    }

    public function getEmployesNonAssigne(Prestation $prestation){
        $subQueryBuilder = $this->getEntityManager()->createQueryBuilder();
        $subquery = $subQueryBuilder            
            ->select('et')
            ->from('App\Entity\Employes','et')
            ->innerJoin('et.PrestationsAssignees','p')
            ->where('p.id = :id')
            ->orderBy('et.id', 'ASC')
            ;
            
            $queryBuildertmp = $this->getEntityManager()->createQueryBuilder();
            $query = $queryBuildertmp
            ->select('e')
            ->from('App\Entity\Employes','e')
            ->where($queryBuildertmp->expr()->notIn('e.id',$subquery->getDQL()))
            ->setParameter('id', $prestation->getId())
            ->getQuery()
            ;
            $result = $query->getResult();
            return $result;
            

    }



    public function getEmployesAssigne(Prestation $prestation){
        $subQueryBuilder = $this->getEntityManager()->createQueryBuilder();
        $query = $subQueryBuilder            
            ->select('et')
            ->from('App\Entity\Employes','et')
            ->innerJoin('et.PrestationsAssignees','p')
            ->where('p.id = :id')
            ->setParameter('id', $prestation->getId())
            ->getQuery()
            ;
           
            $result = $query->getResult();
            return $result;
            

    }



    // /**
    //  * @return Employes[] Returns an array of Employes objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Employes
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

<?php

namespace App\Repository;

use App\Entity\Prestation;
use App\Entity\MesPrestation;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method MesPrestation|null find($id, $lockMode = null, $lockVersion = null)
 * @method MesPrestation|null findOneBy(array $criteria, array $orderBy = null)
 * @method MesPrestation[]    findAll()
 * @method MesPrestation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MesPrestationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MesPrestation::class);
    }

     /**
      * @return MesPrestation[] Returns an array of MesPrestation objects
      */
    
    public function getAllId()
    {
        $subQueryBuilder = $this->getEntityManager()->createQueryBuilder();
        return $subQueryBuilder 
            ->select('p.id')
            ->from('App\Entity\MesPrestation','p')
            ->getQuery()
            ->getResult()
        ;

    }
    public function findNamesById(Prestation $prestation){
        $subQueryBuilder = $this->getEntityManager()->createQueryBuilder();
        $query = $subQueryBuilder            
            ->select('mp.nomPrestation')
            ->from('App\Entity\MesPrestation','em')
            ->innerJoin('App\Entity\MesPrestation','p')
            ->where('p.id = :id')
            ->and('p.')
            ->orderBy('et.id', 'ASC')
            ;
            $result = $query->getResult();
            return $result;
            

    }
    }
    

    /*
    public function findOneBySomeField($value): ?MesPrestation
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */


<?php

namespace App\Repository;

use App\Entity\Prestation;
use App\Entity\Employes;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Prestation|null find($id, $lockMode = null, $lockVersion = null)
 * @method Prestation|null findOneBy(array $criteria, array $orderBy = null)
 * @method Prestation[]    findAll()
 * @method Prestation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PrestationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Prestation::class);
    }



    public function myFindById(Prestation $prestation): array
    {

        $entityManager = $this->getEntityManager();
        $query = $entityManager->createQuery(
            'SELECT E
            from App\Entity\Employes E
            where E.id not in (
                SELECT P.employesId
                FROM prestation_employes P
                where P.prestationId = :id 
                )

            '

        )->setParameter('id',$prestation->getId());


        return $query->getResult();
    }


    public function getDetailPrestationReservees(Prestation $prestation): array
     {

        $entityManager = $this->getEntityManager();
        $query = $entityManager->createQuery(
            'SELECT m.prestationReserve
            from App\Entity\Prestation m
            where m.id = :id
            
            '
        )->setParameter('id',$prestation->getId());
        $ids = $query->getResult();
        $result=[];
       // var_dump($ids);
        for ($i = 0;$i < count($ids[0]['prestationReserve']) ; $i++){
            
            $entityManager = $this->getEntityManager();
            $query = $entityManager->createQuery(
            'SELECT m
            from App\Entity\MesPrestation m
            where m.id = :id'
            )->setParameter('id',$ids[0]['prestationReserve'][$i]["id"]);
            $res =  $query->getResult();
            $result [] = [$res[0]->getId(),$res[0]->getNomPrestation(),$res[0]->getNombreEmployes(),number_format($res[0]->getPrix(),2,'.',' ')];
        }
        return $result;

     }
    // /**
    //  * @return Prestation[] Returns an array of Prestation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Prestation
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

# AlloDjari

Informations générales sur le projet dans le [wiki](https://gitlab.com/hugogil/allodjari/-/wikis/home).  

## Installer le projet AlloDjari

Étapes préliminaires:

* Disposer d'un terminal avec php et nodeJS, les droits administrateurs
* Cloner le dépôt et s'y placer (`cd allodjari`).

### Back
1. `cd server`
2. `composer install`
    * installation des dépendances
3. Configurer la base de données dans le fichier .env `DATABASE_URL` (changement à ne pas commit), la configuration fonctionne avec la configuration MySQL par défaut de WampServer 
4. Si vous n'avez pas la base de données allodjari, ou si c'est votre première fois: `php bin/console doctrine:database:create`, cela va créer la BDD "allodjari"
5. Lancer les migrations pour mettre à jour votre base de données: `php bin/console doctrine:migrations:migrate`
6. Lancer `php bin/console doctrine:fixtures:load --no-interaction` pour avoir un jeu de données
7. Pour lancer le serveur du back: `php bin/console server:run`, le serveur sera accessible sur **localhost:8000**
8. Installer OpenSSL en suivant cette [video](https://www.youtube.com/watch?v=892_4UQy_L8) (version .zip d'OpenSSL: [télécharger ici](https://sourceforge.net/projects/openssl/files/latest/download))
9. Créer le répertoire config/jwt: `mkdir config/jwt`
10. Exécuter la commande `openssl genrsa -out config/jwt/private.pem -aes256 4096`, une Passphrase sera demandé, copiez la `JWT_PASSPHRASE` qui est dans `/.env`
11. Exécuter la commande `openssl rsa -pubout -in config/jwt/private.pem -out config/jwt/public.pem`, une Passphrase sera demandé, copiez la `JWT_PASSPHRASE` qui est dans `/.env`

### Front
1. `cd client`
2. `npm install`
    * installation des dépendances, à ne faire que lors de la première installation ou lorsqu'elles changent (dans le doute, faites-le)

## Lancer le projet AlloDjari

Il est nécessaire de lancer deux consoles pour faire tourner d'une part le back, puis le front.
* `cd server && php bin/console server:run`
* `cd client && npm start`
    * :warning: il faut que le back-end soit lancé sur le port 8000 pour que le build fonctionne.
* Le client est alors accessible sur `http://localhost:8001`.
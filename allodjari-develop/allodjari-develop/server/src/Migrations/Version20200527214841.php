<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200527214841 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE feed_back (id INT AUTO_INCREMENT NOT NULL, id_user_id INT NOT NULL, note INT NOT NULL, comment VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_ED592A6079F37AE5 (id_user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE feed_back_description_pro (feed_back_id INT NOT NULL, description_pro_id INT NOT NULL, INDEX IDX_11B51B23534A2B85 (feed_back_id), INDEX IDX_11B51B238DA94B32 (description_pro_id), PRIMARY KEY(feed_back_id, description_pro_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE feed_back ADD CONSTRAINT FK_ED592A6079F37AE5 FOREIGN KEY (id_user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE feed_back_description_pro ADD CONSTRAINT FK_11B51B23534A2B85 FOREIGN KEY (feed_back_id) REFERENCES feed_back (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE feed_back_description_pro ADD CONSTRAINT FK_11B51B238DA94B32 FOREIGN KEY (description_pro_id) REFERENCES description_pro (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE feed_back_description_pro DROP FOREIGN KEY FK_11B51B23534A2B85');
        $this->addSql('DROP TABLE feed_back');
        $this->addSql('DROP TABLE feed_back_description_pro');
    }
}

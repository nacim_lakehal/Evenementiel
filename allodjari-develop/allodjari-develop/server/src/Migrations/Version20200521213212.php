<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200521213212 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE contacter DROP FOREIGN KEY FK_B9AE2AD9EAB5DEB');
        $this->addSql('DROP INDEX IDX_B9AE2AD9EAB5DEB ON contacter');
        $this->addSql('ALTER TABLE contacter CHANGE many_to_one_id id_offer_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE contacter ADD CONSTRAINT FK_B9AE2AD931D987B FOREIGN KEY (id_offer_id) REFERENCES offer (id)');
        $this->addSql('CREATE INDEX IDX_B9AE2AD931D987B ON contacter (id_offer_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE contacter DROP FOREIGN KEY FK_B9AE2AD931D987B');
        $this->addSql('DROP INDEX IDX_B9AE2AD931D987B ON contacter');
        $this->addSql('ALTER TABLE contacter CHANGE id_offer_id many_to_one_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE contacter ADD CONSTRAINT FK_B9AE2AD9EAB5DEB FOREIGN KEY (many_to_one_id) REFERENCES offer (id)');
        $this->addSql('CREATE INDEX IDX_B9AE2AD9EAB5DEB ON contacter (many_to_one_id)');
    }
}

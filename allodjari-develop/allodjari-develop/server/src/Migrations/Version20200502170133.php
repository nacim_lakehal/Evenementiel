<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200502170133 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql('CREATE TABLE description_pro (id INT AUTO_INCREMENT NOT NULL, id_user INT NOT NULL, nb_mission INT NOT NULL, description LONGTEXT DEFAULT NULL, dat_debut DATETIME NOT NULL, rue VARCHAR(255) NOT NULL, complement_adresse VARCHAR(255) DEFAULT NULL, cod_postal VARCHAR(6) NOT NULL, ville VARCHAR(100) NOT NULL, lien_photo1 VARCHAR(255) NOT NULL, lphoto2 VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE description_pro ADD lien_photo3 VARCHAR(255) NOT NULL, ADD lien_photo4 VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE description_pro ADD lien_photo2 VARCHAR(255) DEFAULT NULL, DROP lphoto2, CHANGE lien_photo1 lien_photo1 VARCHAR(255) DEFAULT NULL, CHANGE lien_photo3 lien_photo3 VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE description_pro ADD descriptions_pro_id INT NOT NULL');
        $this->addSql('ALTER TABLE description_pro ADD CONSTRAINT FK_DA0D992A7770619D FOREIGN KEY (descriptions_pro_id) REFERENCES profession (id)');
        $this->addSql('CREATE INDEX IDX_DA0D992A7770619D ON description_pro (descriptions_pro_id)');
        $this->addSql('ALTER TABLE description_pro DROP FOREIGN KEY FK_DA0D992A7770619D');
        $this->addSql('DROP INDEX IDX_DA0D992A7770619D ON description_pro');
        $this->addSql('ALTER TABLE description_pro CHANGE descriptions_pro_id id_profession_id INT NOT NULL');
        $this->addSql('ALTER TABLE description_pro ADD CONSTRAINT FK_DA0D992A43C758F3 FOREIGN KEY (id_profession_id) REFERENCES profession (id)');
        $this->addSql('CREATE INDEX IDX_DA0D992A43C758F3 ON description_pro (id_profession_id)');
        $this->addSql('ALTER TABLE description_pro DROP id_user');
        $this->addSql('ALTER TABLE description_pro ADD id_user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE description_pro ADD CONSTRAINT FK_DA0D992A79F37AE5 FOREIGN KEY (id_user_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_DA0D992A79F37AE5 ON description_pro (id_user_id)');



        $this->addSql('ALTER TABLE description_pro ADD created_at DATETIME NOT NULL, ADD updated_at DATETIME NOT NULL');

    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql('DROP TABLE description_pro');

    }
}

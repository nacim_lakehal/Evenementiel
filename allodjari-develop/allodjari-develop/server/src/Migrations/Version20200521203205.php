<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200521203205 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE contacter (id INT AUTO_INCREMENT NOT NULL, id_user_sender_id INT NOT NULL, id_user_receiver_id INT NOT NULL, id_desc_pro_id INT DEFAULT NULL, many_to_one_id INT DEFAULT NULL, UNIQUE INDEX UNIQ_B9AE2AD9CBA2837F (id_user_sender_id), UNIQUE INDEX UNIQ_B9AE2AD987C271F2 (id_user_receiver_id), INDEX IDX_B9AE2AD994F4D29A (id_desc_pro_id), INDEX IDX_B9AE2AD9EAB5DEB (many_to_one_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE contacter ADD CONSTRAINT FK_B9AE2AD9CBA2837F FOREIGN KEY (id_user_sender_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE contacter ADD CONSTRAINT FK_B9AE2AD987C271F2 FOREIGN KEY (id_user_receiver_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE contacter ADD CONSTRAINT FK_B9AE2AD994F4D29A FOREIGN KEY (id_desc_pro_id) REFERENCES description_pro (id)');
        $this->addSql('ALTER TABLE contacter ADD CONSTRAINT FK_B9AE2AD9EAB5DEB FOREIGN KEY (many_to_one_id) REFERENCES offer (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE contacter');
    }
}

<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200524001553 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE contacter DROP INDEX UNIQ_B9AE2AD9CBA2837F, ADD INDEX IDX_B9AE2AD9CBA2837F (id_user_sender_id)');
        $this->addSql('ALTER TABLE contacter DROP INDEX UNIQ_B9AE2AD987C271F2, ADD INDEX IDX_B9AE2AD987C271F2 (id_user_receiver_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE contacter DROP INDEX IDX_B9AE2AD9CBA2837F, ADD UNIQUE INDEX UNIQ_B9AE2AD9CBA2837F (id_user_sender_id)');
        $this->addSql('ALTER TABLE contacter DROP INDEX IDX_B9AE2AD987C271F2, ADD UNIQUE INDEX UNIQ_B9AE2AD987C271F2 (id_user_receiver_id)');
    }
}

<?php

namespace App\Repository;

use App\Entity\DescriptionPro;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method DescriptionPro|null find($id, $lockMode = null, $lockVersion = null)
 * @method DescriptionPro|null findOneBy(array $criteria, array $orderBy = null)
 * @method DescriptionPro[]    findAll()
 * @method DescriptionPro[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DescriptionProRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DescriptionPro::class);
    }

    // /**
    //  * @return DescriptionPro[] Returns an array of DescriptionPro objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DescriptionPro
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

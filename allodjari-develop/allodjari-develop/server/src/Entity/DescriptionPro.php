<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;



/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\DescriptionProRepository")
 */
class DescriptionPro
{

    /*
     * TimestampableEntity trait
     */
    use TimestampableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;



    /**
     * @ORM\Column(type="integer")
     */
    private $NbMission;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $Description;

    /**
     * @ORM\Column(type="datetime")
     */
    private $datDebut;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $rue;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $complementAdresse;

    /**
     * @ORM\Column(type="string", length=6)
     */
    private $codPostal;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $ville;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $LienPhoto1;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $LienPhoto2;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $LienPhoto3;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $LienPhoto4;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="IdProfession")
     */
    private $IdUser;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Profession", inversedBy="descriptionPros")
     */
    private $IdProfession;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Contacter", mappedBy="IdDescPro")
     */
    private $contacters;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\FeedBack", mappedBy="IdDescriptionPro")
     */
    private $feedBacks;


    public function __construct()
    {
        $this->IdOffer = new ArrayCollection();
        $this->contacters = new ArrayCollection();
        $this->feedBacks = new ArrayCollection();
    }




    public function getId(): ?int
    {
        return $this->id;
    }


    public function getNbMission(): ?int
    {
        return $this->NbMission;
    }

    public function setNbMission(int $NbMission): self
    {
        $this->NbMission = $NbMission;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->Description;
    }

    public function setDescription(?string $Description): self
    {
        $this->Description = $Description;

        return $this;
    }

    public function getDatDebut(): ?\DateTimeInterface
    {
        return $this->datDebut;
    }

    public function setDatDebut(\DateTimeInterface $datDebut): self
    {
        $this->datDebut = $datDebut;

        return $this;
    }

    public function getRue(): ?string
    {
        return $this->rue;
    }

    public function setRue(string $rue): self
    {
        $this->rue = $rue;

        return $this;
    }

    public function getComplementAdresse(): ?string
    {
        return $this->complementAdresse;
    }

    public function setComplementAdresse(?string $complementAdresse): self
    {
        $this->complementAdresse = $complementAdresse;

        return $this;
    }

    public function getCodPostal(): ?string
    {
        return $this->codPostal;
    }

    public function setCodPostal(string $codPostal): self
    {
        $this->codPostal = $codPostal;

        return $this;
    }

    public function getVille(): ?string
    {
        return $this->ville;
    }

    public function setVille(string $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    public function getLienPhoto1(): ?string
    {
        return $this->LienPhoto1;
    }

    public function setLienPhoto1(string $LienPhoto1): self
    {
        $this->LienPhoto1 = $LienPhoto1;

        return $this;
    }

    public function getLienPhoto2(): ?string
    {
        return $this->LienPhoto2;
    }

    public function setLienPhoto2(string $LienPhoto2): self
    {
        $this->$LienPhoto2 = $LienPhoto2;

        return $this;
    }

    public function getLienPhoto3(): ?string
    {
        return $this->LienPhoto3;
    }

    public function setLienPhoto3(string $LienPhoto3): self
    {
        $this->LienPhoto3 = $LienPhoto3;

        return $this;
    }

    public function getLienPhoto4(): ?string
    {
        return $this->LienPhoto4;
    }

    public function setLienPhoto4(?string $LienPhoto4): self
    {
        $this->LienPhoto4 = $LienPhoto4;

        return $this;
    }

    public function getIdUser(): ?User
    {
        return $this->IdUser;
    }

    public function setIdUser(?User $IdUser): self
    {
        $this->IdUser = $IdUser;

        return $this;
    }

    public function getIdProfession(): ?Profession
    {
        return $this->IdProfession;
    }

    public function setIdProfession(?Profession $IdProfession): self
    {
        $this->IdProfession = $IdProfession;

        return $this;
    }

    /**
     * @return Collection|Contacter[]
     */
    public function getContacters(): Collection
    {
        return $this->contacters;
    }

    public function addContacter(Contacter $contacter): self
    {
        if (!$this->contacters->contains($contacter)) {
            $this->contacters[] = $contacter;
            $contacter->setIdDescPro($this);
        }

        return $this;
    }

    public function removeContacter(Contacter $contacter): self
    {
        if ($this->contacters->contains($contacter)) {
            $this->contacters->removeElement($contacter);
            // set the owning side to null (unless already changed)
            if ($contacter->getIdDescPro() === $this) {
                $contacter->setIdDescPro(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|FeedBack[]
     */
    public function getFeedBacks(): Collection
    {
        return $this->feedBacks;
    }

    public function addFeedBack(FeedBack $feedBack): self
    {
        if (!$this->feedBacks->contains($feedBack)) {
            $this->feedBacks[] = $feedBack;
            $feedBack->addIdDescriptionPro($this);
        }

        return $this;
    }

    public function removeFeedBack(FeedBack $feedBack): self
    {
        if ($this->feedBacks->contains($feedBack)) {
            $this->feedBacks->removeElement($feedBack);
            $feedBack->removeIdDescriptionPro($this);
        }

        return $this;
    }




}

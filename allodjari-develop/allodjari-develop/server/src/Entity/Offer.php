<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ApiResource
 * @ApiFilter(SearchFilter::class, properties={"id": "exact", "title": "partial", "idUser": "exact", "idProfession": "exact"})
 * @ORM\Entity(repositoryClass="App\Repository\OfferRepository")
 */
class Offer
{
    use TimestampableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="offers")
     * @ORM\JoinColumn(nullable=false)
     */
    private $idUser;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Profession", inversedBy="offers")
     */
    private $idProfession;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $beginDate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $picture;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Contacter", mappedBy="IdOffer")
     */
    private $Contacter;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Category", inversedBy="offers")
     * @ORM\JoinColumn(nullable=false)
     */
    private $category;



    public function __construct()
    {
        $this->idProfession = new ArrayCollection();
        $this->contacters = new ArrayCollection();
        $this->Contacter = new ArrayCollection();
    }

    public function getId(): ?int

    {
        return $this->id;
    }

    public function getIdUser(): ?User
    {
        return $this->idUser;
    }

    public function setIdUser(?User $idUser): self
    {
        $this->idUser = $idUser;

        return $this;
    }

    /**
     * @return Collection|Profession[]
     */
    public function getIdProfession(): Collection
    {
        return $this->idProfession;
    }

    public function addIdProfession(Profession $idProfession): self
    {
        if (!$this->idProfession->contains($idProfession)) {
            $this->idProfession[] = $idProfession;
        }

        return $this;
    }

    public function removeIdProfession(Profession $idProfession): self
    {
        if ($this->idProfession->contains($idProfession)) {
            $this->idProfession->removeElement($idProfession);
        }

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getBeginDate(): ?\DateTimeInterface
    {
        return $this->beginDate;
    }

    public function setBeginDate(?\DateTimeInterface $beginDate): self
    {
        $this->beginDate = $beginDate;

        return $this;
    }

    public function getPicture(): ?string
    {
        return $this->picture;
    }

    public function setPicture(?string $picture): self
    {
        $this->picture = $picture;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return Collection|Contacter[]
     */
    public function getContacter(): Collection
    {
        return $this->Contacter;
    }

    public function addContacter(Contacter $contacter): self
    {
        if (!$this->Contacter->contains($contacter)) {
            $this->Contacter[] = $contacter;
            $contacter->setIdOffer($this);
        }

        return $this;
    }

    public function removeContacter(Contacter $contacter): self
    {
        if ($this->Contacter->contains($contacter)) {
            $this->Contacter->removeElement($contacter);
            // set the owning side to null (unless already changed)
            if ($contacter->getIdOffer() === $this) {
                $contacter->setIdOffer(null);
            }
        }

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }
}

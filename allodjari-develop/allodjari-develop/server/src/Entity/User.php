<?php

namespace App\Entity;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ApiResource(
 *     normalizationContext={"groups"={"unsecured"}}
 * )
 * @ApiFilter(SearchFilter::class, properties={"id": "exact", "name": "partial", "lastName": "partial", "gender": "exact", "phoneNumber": "exact", "email": "exact"})
 * @UniqueEntity("phoneNumber",message="Un utilisateur ayant ce numéro de téléphone existe déjà.")
 * @UniqueEntity("email",message="Un utilisateur ayant cette adresse email existe déjà.")
 */
class User implements UserInterface
{
    /*
     * TimestampableEntity trait
     */
    use TimestampableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups("unsecured")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Le prénom doit être renseingé.")
     * @Groups("unsecured")
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Le nom doit être renseingé.")
     * @Groups("unsecured")
     */
    private $lastName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups("unsecured")
     */
    private $adress;

    /**
     * @ORM\Column(type="boolean")
     * @Assert\NotBlank(message="Le sexe doit être renseingé.")
     * @Groups("unsecured")
     */
    private $gender;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Assert\DateTime(message="Format de date de naissance invalide.")
     * @Groups("unsecured")
     */
    private $birthDate;

    /**
     * @ORM\Column(type="string", length=20)
     * @Assert\NotBlank(message="Le numéro de téléphone doit être renseigné.")
     * @Assert\Length(min=9,minMessage="Le numéro de téléphone renseigné est très court.", max=20,maxMessage="Le numéro de téléphone renseigné est très long.")
     * @Groups("unsecured")
     */
    private $phoneNumber;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Email(message="L'adresse email doit avoir un format valide.")
     * @Assert\NotBlank(message="L'adresse email doit être renseignée.")
     * @Groups("unsecured")
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $password;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DescriptionPro", mappedBy="idUser")
     */
    private $IsPro;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Offer", mappedBy="idUser", orphanRemoval=true)
     */
    private $offers;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DescriptionPro", mappedBy="IdUser")
     */
    private $IdProfession;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\FeedBack", mappedBy="IdUser")
     */
    private $FeedBacks;

    public function __construct()
    {
        $this->IsPro = new ArrayCollection();
        $this->offers = new ArrayCollection();
        $this->IdProfession = new ArrayCollection();
        $this->FeedBacks = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getAdress(): ?string
    {
        return $this->adress;
    }

    public function setAdress(?string $adress): self
    {
        $this->adress = $adress;

        return $this;
    }

    public function getGender(): ?bool
    {
        return $this->gender;
    }

    public function setGender(bool $gender): self
    {
        $this->gender = $gender;

        return $this;
    }

    public function getBirthDate(): ?\DateTimeInterface
    {
        return $this->birthDate;
    }

    public function setBirthDate(?\DateTimeInterface $birthDate): self
    {
        $this->birthDate = $birthDate;

        return $this;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(string $phoneNumber): self
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getRoles()
    {
        return ['USER_ROLE'];
    }

    /**
     * @inheritDoc
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    public function getUsername()
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    public function eraseCredentials()
    {
        return null;
    }

    /**
     * @return Collection|DescriptionPro[]
     */
    public function getIsPro(): Collection
    {
        return $this->IsPro;
    }

    public function addIsPro(DescriptionPro $isPro): self
    {
        if (!$this->IsPro->contains($isPro)) {
            $this->IsPro[] = $isPro;
            $isPro->setIdUser($this);
        }

        return $this;
    }

    public function removeIsPro(DescriptionPro $isPro): self
    {
        if ($this->IsPro->contains($isPro)) {
            $this->IsPro->removeElement($isPro);
            // set the owning side to null (unless already changed)
            if ($isPro->getIdUser() === $this) {
                $isPro->setIdUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Offer[]
     */
    public function getOffers(): Collection
    {
        return $this->offers;
    }

    public function addOffer(Offer $offer): self
    {
        if (!$this->offers->contains($offer)) {
            $this->offers[] = $offer;
            $offer->setIdUser($this);
        }

        return $this;
    }

    public function removeOffer(Offer $offer): self
    {
        if ($this->offers->contains($offer)) {
            $this->offers->removeElement($offer);
            // set the owning side to null (unless already changed)
            if ($offer->getIdUser() === $this) {
                $offer->setIdUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|DescriptionPro[]
     */
    public function getIdProfession(): Collection
    {
        return $this->IdProfession;
    }

    public function addIdProfession(DescriptionPro $idProfession): self
    {
        if (!$this->IdProfession->contains($idProfession)) {
            $this->IdProfession[] = $idProfession;
            $idProfession->setIdUser($this);
        }

        return $this;
    }

    public function removeIdProfession(DescriptionPro $idProfession): self
    {
        if ($this->IdProfession->contains($idProfession)) {
            $this->IdProfession->removeElement($idProfession);
            // set the owning side to null (unless already changed)
            if ($idProfession->getIdUser() === $this) {
                $idProfession->setIdUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|FeedBack[]
     */
    public function getFeedBacks(): Collection
    {
        return $this->FeedBacks;
    }

    public function addFeedBack(FeedBack $feedBack): self
    {
        if (!$this->FeedBacks->contains($feedBack)) {
            $this->FeedBacks[] = $feedBack;
            $feedBack->setIdUser($this);
        }

        return $this;
    }

    public function removeFeedBack(FeedBack $feedBack): self
    {
        if ($this->FeedBacks->contains($feedBack)) {
            $this->FeedBacks->removeElement($feedBack);
            // set the owning side to null (unless already changed)
            if ($feedBack->getIdUser() === $this) {
                $feedBack->setIdUser(null);
            }
        }

        return $this;
    }
}

<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ApiResource
 * @ORM\Entity(repositoryClass="App\Repository\ProfessionRepository")
 */
class Profession
{
    /*
     * TimestampableEntity trait
     */
    use TimestampableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @ORM\OneToMany(targetEntity="App\Entity\DescriptionPro", mappedBy="DescriptionsPro")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $label;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Offer", mappedBy="idProfession")
     */
    private $offers;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DescriptionPro", mappedBy="IdProfession")
     */
    private $descriptionPros;



    public function __construct()
    {
        $this->idProfession = new ArrayCollection();
        $this->offers = new ArrayCollection();
        $this->descriptionPros = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDescription(): ?string
    {
        return $this->Description;
    }

    public function setDescription(string $Description): self
    {
        $this->Description = $Description;

        return $this;
    }

    /**
     * @return Collection|DescriptionPro[]
     */
    public function getIdProfession(): Collection
    {
        return $this->idProfession;
    }

    public function addIdProfession(DescriptionPro $idProfession): self
    {
        if (!$this->idProfession->contains($idProfession)) {
            $this->idProfession[] = $idProfession;
            $idProfession->setDescriptionsPro($this);
        }

        return $this;
    }

    public function removeIdProfession(DescriptionPro $idProfession): self
    {
        if ($this->idProfession->contains($idProfession)) {
            $this->idProfession->removeElement($idProfession);
            // set the owning side to null (unless already changed)
            if ($idProfession->getDescriptionsPro() === $this) {
                $idProfession->setDescriptionsPro(null);
            }
        }

        return $this;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    /**
     * @return Collection|Offer[]
     */
    public function getOffers(): Collection
    {
        return $this->offers;
    }

    public function addOffer(Offer $offer): self
    {
        if (!$this->offers->contains($offer)) {
            $this->offers[] = $offer;
            $offer->addIdProfession($this);
        }

        return $this;
    }

    public function removeOffer(Offer $offer): self
    {
        if ($this->offers->contains($offer)) {
            $this->offers->removeElement($offer);
            $offer->removeIdProfession($this);
        }

        return $this;
    }

    /**
     * @return Collection|DescriptionPro[]
     */
    public function getDescriptionPros(): Collection
    {
        return $this->descriptionPros;
    }

    public function addDescriptionPro(DescriptionPro $descriptionPro): self
    {
        if (!$this->descriptionPros->contains($descriptionPro)) {
            $this->descriptionPros[] = $descriptionPro;
            $descriptionPro->setIdProfession($this);
        }

        return $this;
    }

    public function removeDescriptionPro(DescriptionPro $descriptionPro): self
    {
        if ($this->descriptionPros->contains($descriptionPro)) {
            $this->descriptionPros->removeElement($descriptionPro);
            // set the owning side to null (unless already changed)
            if ($descriptionPro->getIdProfession() === $this) {
                $descriptionPro->setIdProfession(null);
            }
        }

        return $this;
    }
}

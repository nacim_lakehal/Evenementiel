<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\ContacterRepository")
 */
class Contacter
{

    use TimestampableEntity;
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $IdUserSender;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $IdUserReceiver;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\DescriptionPro", inversedBy="contacters")
     */
    private $IdDescPro;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Offer", inversedBy="Contacter")
     */
    private $IdOffer;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdUserSender(): ?User
    {
        return $this->IdUserSender;
    }

    public function setIdUserSender(User $IdUserSender): self
    {
        $this->IdUserSender = $IdUserSender;

        return $this;
    }

    public function getIdUserReceiver(): ?User
    {
        return $this->IdUserReceiver;
    }

    public function setIdUserReceiver(User $IdUserReceiver): self
    {
        $this->IdUserReceiver = $IdUserReceiver;

        return $this;
    }

    public function getIdDescPro(): ?DescriptionPro
    {
        return $this->IdDescPro;
    }

    public function setIdDescPro(?DescriptionPro $IdDescPro): self
    {
        $this->IdDescPro = $IdDescPro;

        return $this;
    }

    public function getIdOffer(): ?Offer
    {
        return $this->IdOffer;
    }

    public function setIdOffer(?Offer $IdOffer): self
    {
        $this->IdOffer = $IdOffer;

        return $this;
    }






}

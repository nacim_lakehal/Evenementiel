<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FeedBackRepository")
 * @ApiResource
 */
class FeedBack
{
    use TimestampableEntity;
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="FeedBacks")
     * @ORM\JoinColumn(nullable=false)
     */
    private $IdUser;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\DescriptionPro", inversedBy="feedBacks")
     */
    private $IdDescriptionPro;

    /**
     * @ORM\Column(type="integer")
     */
    private $note;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $comment;

    public function __construct()
    {
        $this->IdDescriptionPro = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdUser(): ?User
    {
        return $this->IdUser;
    }

    public function setIdUser(?User $IdUser): self
    {
        $this->IdUser = $IdUser;

        return $this;
    }

    /**
     * @return Collection|DescriptionPro[]
     */
    public function getIdDescriptionPro(): Collection
    {
        return $this->IdDescriptionPro;
    }

    public function addIdDescriptionPro(DescriptionPro $idDescriptionPro): self
    {
        if (!$this->IdDescriptionPro->contains($idDescriptionPro)) {
            $this->IdDescriptionPro[] = $idDescriptionPro;
        }

        return $this;
    }

    public function removeIdDescriptionPro(DescriptionPro $idDescriptionPro): self
    {
        if ($this->IdDescriptionPro->contains($idDescriptionPro)) {
            $this->IdDescriptionPro->removeElement($idDescriptionPro);
        }

        return $this;
    }

    public function getNote(): ?int
    {
        return $this->note;
    }

    public function setNote(int $note): self
    {
        $this->note = $note;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }
}

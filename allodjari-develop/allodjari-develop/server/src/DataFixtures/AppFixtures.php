<?php


namespace App\DataFixtures;
use App\Entity\Category;
use App\Entity\DescriptionPro;
use App\Entity\Offer;
use App\Entity\Profession;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    /*
     *
     * @var UserPasswordEncoderInterface
     * */
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');
        // create some professions
        $profession1 = new Profession();
        $profession1->setDescription('Medecin généraliste')
                    ->setLabel('Corp medical')
                    ->setCreatedAt(new \DateTime('now'))
                    ->setUpdatedAt(new \DateTime('now'));
        $manager->persist($profession1);
        // create some professions
        $profession2 = new Profession();
        $profession2->setDescription('mécanicien')
            ->setLabel('mécanique')
            ->setCreatedAt(new \DateTime('now'))
            ->setUpdatedAt(new \DateTime('now'));
        $manager->persist($profession2);
        $profession3 = new Profession();
        $profession3->setDescription('avocat')
            ->setLabel('avocat libéral')
            ->setCreatedAt(new \DateTime('now'))
            ->setUpdatedAt(new \DateTime('now'));
        $manager->persist($profession3);
        $profession4 = new Profession();
        $profession4->setDescription('écrivain')
            ->setLabel('avocat libéral')
            ->setCreatedAt(new \DateTime('now'))
            ->setUpdatedAt(new \DateTime('now'));
        $manager->persist($profession4);
        $listPro= [$profession1,$profession2,$profession3,$profession4];

        // Creating categories:
        $category1 = new Category();
        $category1->setName("Mécanique");
        $manager->persist($category1);
        // create some professions
        $category2 = new Category();
        $category2->setName("Entretien");
        $manager->persist($category2);
        $category3 = new Category();
        $category3->setName("Réparation");
        $manager->persist($category3);
        $category4 = new Category();
        $category4->setName("Transport");
        $manager->persist($category4);
        $listCategory = [$category1,$category2,$category3,$category4];

        // create 20 products! Bam!
        for ($i = 0; $i < 20; $i++) {

            $user = new User();
            $hash = $this->encoder->encodePassword($user, "testtest");
            $user->setName($faker->firstName())
                ->setLastName($faker->lastName())
                ->setAdress($faker->address())
                ->setBirthDate($faker->dateTime())
                ->setEmail($faker->safeEmail())
                ->setPhoneNumber($faker->phoneNumber)
                ->setGender(0)
                ->setPassword($hash);
            $manager->persist($user);

            for ($j = 0; $j < 3; $j++) {
                $randIndex = array_rand($listPro);
                $descPro = new DescriptionPro();
                $descPro->setNbMission($faker->numberBetween($min = 1, $max = 10))
                    ->setDescription($faker->text)
                    ->setDatDebut($faker->dateTimeInInterval($startDate = '-30 days', $interval = '+ 50 days'))
                    ->setRue($faker->streetName)
                    ->setComplementAdresse($faker->secondaryAddress)
                    ->setCodPostal($faker->postcode)
                    ->setVille($faker->city)
                    ->setIdUser($user)
                    ->setCreatedAt(new \DateTime('now'))
                    ->setUpdatedAt(new \DateTime('now'))
                    ->setIdProfession($listPro[$randIndex]);
                $manager->persist($descPro);
            }
        }
            for ($i = 0; $i < 20; $i++) {

                $user = new User();
                $hash = $this->encoder->encodePassword($user,"testtest");
                $user->setName($faker->firstName())
                    ->setLastName($faker->lastName())
                    ->setAdress($faker->address())
                    ->setBirthDate($faker->dateTime())
                    ->setEmail($faker->safeEmail())
                    ->setPhoneNumber($faker->phoneNumber)
                    ->setGender(0)
                    ->setPassword($hash);
                $manager->persist($user);

                for($j=0;$j<3;$j++){
                    $randIndex = array_rand($listPro);
                    $randCategory = $listCategory[array_rand($listCategory)];
                    $offer = new Offer();
                    $offer->setTitle($faker->sentence($nbWords = 2, $variableNbWords = true))
                          ->setIdUser($user)
                          ->setDescription($faker->sentence($nbWords = 14, $variableNbWords = true))
                          ->setBeginDate($faker->dateTimeInInterval($startDate = '-10 days', $interval = '+ 20 days'))
                          ->setCity($faker->city)
                          ->setCreatedAt($faker->dateTimeInInterval($startDate = '-10 days', $interval = '+ 20 days'))
                          ->setUpdatedAt($faker->dateTimeInInterval($startDate = '-10 days', $interval = '+ 20 days'))
                          ->setCategory($randCategory);
                    $offer->addIdProfession($listPro[$randIndex]);
                    $offer->addIdProfession($listPro[$randIndex]);


                    $manager->persist($offer);
                }

        }

        $manager->flush();
    }
}
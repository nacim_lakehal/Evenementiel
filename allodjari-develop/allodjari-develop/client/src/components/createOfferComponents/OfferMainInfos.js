import React, {Component} from "react";
import COStyles from './createOfferStyles.module.scss';
import {ErrorMessage, Field, Form, Formik} from "formik";
import styles from "../UI/form/formikFieldLabel/formikFieldLabel.module.scss";
import PropTypes from "prop-types";
import * as Yup from "yup";
import FormikFieldLabel from "../UI/form/formikFieldLabel/formikFieldLabel";


const OfferMainInfos = ({ values, onSubmit }) => {

    const valuesSchema = Yup.object().shape({
        title: Yup.string().required(),
        city: Yup.string().required()
    });

    return (
        <>
            <Formik initialValues={values} validationSchema={valuesSchema} onSubmit={onSubmit}>
                {({values, setFieldValue}) => (
                    <Form>
                        <div className={styles.container}>
                            <FormikFieldLabel fieldName='title' label='Titre' fieldType='text' placeholder='Jardinage...' />
                            <FormikFieldLabel fieldName='city' label='Ville' fieldType='text' placeholder='Alger' />
                        </div>
                        <button className={COStyles.continueFlowButton} type='submit'>Continuer</button>
                        <br/>
                    </Form>
                )}
            </Formik>
        </>
    )
};

OfferMainInfos.propTypes = {
    values: PropTypes.object.isRequired,
    onSubmit: PropTypes.func.isRequired,
}

export default OfferMainInfos;
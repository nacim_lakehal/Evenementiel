import React from "react";
import COStyles from './createOfferStyles.module.scss';
import {graphql, useStaticQuery} from "gatsby";
import PropTypes from "prop-types";
import {Field, Form, Formik} from "formik";
import * as Yup from "yup";


const ChooseOfferCategory = ({ values, onSubmit }) => {

    const data = useStaticQuery(graphql`
        query {
          api {
           categories {
              edges {
                node {
                  id,
                  name
                }
              }
            }
          }
        }
      `);

    console.log("categories data:", data);

    const valuesSchema = Yup.object().shape({
        category: Yup.string().required()
    });

    return (
        <>
            <Formik initialValues={values} validationSchema={valuesSchema} onSubmit={onSubmit}>
                {({values, setFieldValue}) => (
                    <Form>
                        <div className={COStyles.formPartContainer}>
                            <label className={COStyles.fieldLabel}>Catégorie</label>
                            <Field className={COStyles.selectFieldContainer} name="category" as="div">
                                {
                                    data.api.categories.edges.map(catNode => {
                                        const {id, name} = catNode.node;
                                        return (
                                            <option key={id} value={id} className={COStyles.selectFieldOption}>
                                                {name}
                                            </option>
                                        )
                                    })
                                }
                            </Field>
                        </div>
                        <button className={COStyles.continueFlowButton} type='submit'>Continuer</button>
                    </Form>
                )}
            </Formik>
        </>
    )
};

ChooseOfferCategory.propTypes = {
    values: PropTypes.object.isRequired,
    onSubmit: PropTypes.func.isRequired,
}

export default ChooseOfferCategory;
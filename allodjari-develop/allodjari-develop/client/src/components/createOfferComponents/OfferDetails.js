 import React, {Component} from "react";
import Layout from "components/UI/layout/layout";
import CreateOfferHeader from "./createOfferHeader/createOfferHeader";
import COStyles from './createOfferStyles.module.scss';
import { Link } from "gatsby";
import {ErrorMessage, Form, Formik} from "formik";
import styles from "../UI/form/formikFieldLabel/formikFieldLabel.module.scss";
import PropTypes from "prop-types";
import * as Yup from "yup";
import FormikFieldLabel from "../UI/form/formikFieldLabel/formikFieldLabel";


const OfferDetails = ({ values, onSubmit }) => {

    const valuesSchema = Yup.object().shape({
        description: Yup.string().required(),
    });

    return (
        <>
            <Formik initialValues={values} validationSchema={valuesSchema} onSubmit={onSubmit}>
                {({values, setFieldValue}) => (
                    <Form>
                        <div className={styles.container}>
                            <FormikFieldLabel fieldName='description' label='Description' placeholder='Je souhaite faire couper ma haie...' fieldType='text' />
                        </div>
                        <button className={COStyles.continueFlowButton} type='submit'>Continuer</button>
                    </Form>
                )}
            </Formik>
        </>
    )
};

OfferDetails.propTypes = {
    values: PropTypes.object.isRequired,
    onSubmit: PropTypes.func.isRequired,
}

export default OfferDetails;
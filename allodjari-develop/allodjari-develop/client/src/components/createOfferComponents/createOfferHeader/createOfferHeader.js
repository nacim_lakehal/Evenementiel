import React  from "react";
import PropTypes from "prop-types";
import styles from './createOfferHeader.module.scss';
import { Link } from "gatsby"
import { FaChevronLeft } from "react-icons/fa";

const CreateOfferHeader = ({ returnPath, stepTitle }) => {
  return (
    <div className={styles.COHeaderContainer}>
      <Link to={returnPath} className={styles.returnLink}><FaChevronLeft size='1rem' />Retour</Link>
      <div className={styles.headerTexts}>
        <span className={styles.headerMainTitle}>Appel d'offre</span>
        <span className={styles.headerStepTitle}>{stepTitle}</span>
      </div>

    </div>
  )
};

CreateOfferHeader.propTypes = {
  returnPath: PropTypes.string.isRequired,
  stepTitle: PropTypes.string.isRequired,
};

export default CreateOfferHeader;
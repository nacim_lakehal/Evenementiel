import React from "react";
import Layout from "components/UI/layout/layout";
import CreateOfferHeader from "./createOfferHeader/createOfferHeader";
import COStyles from './createOfferStyles.module.scss';
import { Link } from "gatsby";


const ProposeOfferCategory = () => {
  return (
    <Layout backgroundColor='#DEDEDE' displayHeader={false}>
      <CreateOfferHeader returnPath="/chooseOfferCategory" stepTitle='Étape 1 bis/4' />
      propose offer category:
      <Link to="/createOffer/setOfferMainInfos" className={COStyles.continueFlowButton}>Continuer</Link>
    </Layout>
  )
};

export default ProposeOfferCategory;
import React from "react";
import Layout from "components/UI/layout/layout";
import style from "./needService.module.scss";
import { Link } from "gatsby"

const NeedService = () => {

  return (
    <Layout backgroundColor="#DEDEDE">
      <h1 className={style.needServiceHeader}>Besoin d'un service ?</h1>
      <div className={style.textInfoContainer}>
        <span>Vous avez besoin d'une compétence précise pour effectuer une tâche ?</span><br/>
        <b>AlloDjari recense tous les professionnels de votre région !</b><br/>
      </div>

      <div className={style.bodyContainer}>
        <Link className={style.goCreateOfferButton} to='/app/createOffer'>
            Je crée un appel d'offre
        </Link>
        <div className={style.buttonDetails}>
          <div>Décrivez précisément votre besoin</div>
          <span>
            Laissez les professionnels venir à vous en remplissant une fiche de mission publique.<br/>
            Vous pourrez choisir parmi les artisans qui vous proposent leurs services.
          </span>
        </div>
        <Link className={style.goProsListButton} to='/app/pros' >
            Je parcours l'annuaire
        </Link>
        <div className={style.buttonDetails}>
          <div>Rencontrez plus de XX professionnels</div>
          <span>
            Sur AlloDjari, il y a forcément quelqu'un qui remplit vos critères.<br/>
            Trouvez-le en quelques clics dans notre annuaire, et hop, <b>AlloDjari</b> ?
          </span>
        </div>
      </div>
    </Layout>
  );
};

export default NeedService;
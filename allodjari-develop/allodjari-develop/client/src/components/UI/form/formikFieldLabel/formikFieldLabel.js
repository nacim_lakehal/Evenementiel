import React from "react"
import { ErrorMessage, Field } from "formik";
import PropTypes from "prop-types";
import styles from './formikFieldLabel.module.scss';

const FormikFieldLabel = ({ fieldName, fieldType, label, placeholder, disabled }) => {

  return (
    <div className={styles.container}>
      <label htmlFor={fieldName} className={styles.label}>{label}</label>
      <Field name={fieldName} type={fieldType} placeholder={placeholder} disabled={disabled} className={styles.field} />
      <ErrorMessage name={fieldName} component='div' className={styles.errorMessage} />
    </div>
  )
}

FormikFieldLabel.propTypes = {
  fieldName: PropTypes.string.isRequired,
  fieldType: PropTypes.oneOf(['tel', 'password', 'text']).isRequired,
  label: PropTypes.string.isRequired,
  placeholder: PropTypes.string.isRequired,
  disabled: PropTypes.bool,
};

export default FormikFieldLabel;
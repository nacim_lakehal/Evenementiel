import React from "react";
import { ErrorMessage, Field } from "formik";
import PropTypes from "prop-types";
import styles from "./formikCheckbox.module.scss";


const CheckedBox = props => (
  <div className={styles.checkedBox}> </div>
);

const Checkbox = ({checked, ...props}) => (
  <div className={styles.checkboxContainer}>
    <input type='checkbox' {...props} />
    {
      checked
      ? <CheckedBox />
      : null
    }
  </div>
);

const FormikCheckbox = ({fieldName, label}) => {

  const [isChecked, setIsChecked] = React.useState(false);

  React.useEffect(() => {
    console.log("isChecked",isChecked);
  }, [isChecked]);

  return (
    <Field name={fieldName}>
      {({ field, meta }) => (
        <>
          <label className={styles.boxAndLabel}>
            <Checkbox
              checked={isChecked}
              onChange={() => {}}
              onClick={() => setIsChecked(!isChecked)}
              {...field}
            />
            <span>{label}</span>
          </label>
          {
            meta.touched && meta.error &&
            <ErrorMessage name={fieldName} component='div' className={styles.errorMessage} />
          }
        </>
      )}
    </Field>
  )
};

FormikCheckbox.propTypes = {
  fieldName: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
};

export default FormikCheckbox;
/**
 * Layout component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.org/docs/use-static-query/
 */

import React from "react"
import PropTypes from "prop-types"
import Header from "../header/header"
import './layout.css'; // base layout from gatsby, basically a css reset
import styles from './layout.module.scss';
import '../../../i18n';

const Layout = ({ children, backgroundColor, displayHeader }) => {

  return (
    <div className={styles.appContainer} style={{backgroundColor: backgroundColor}}>
      {
        displayHeader && <Header siteTitle="AlloDjari" />
      }
      {children}
    </div>
  )
};

Layout.propTypes = {
  children: PropTypes.node.isRequired,
  backgroundColor: PropTypes.string,
  displayHeader: PropTypes.bool,
};

Layout.defaultProps = {
  backgroundColor: '#FFFFFF',
  displayHeader: true,
};

export default Layout;

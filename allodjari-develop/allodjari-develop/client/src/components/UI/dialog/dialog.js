import React, { useEffect } from "react";
import PropTypes from "prop-types";
import style from './dialog.module.scss';
import { FaTimesCircle } from "react-icons/fa";

const Dialog = ( props ) => {

  const { visible, onRequestClose, children, customStyle, showCloseButton } = props;

  useEffect(() => {
    if (!document.body || !document.body.style) return;
    if (visible) document.body.style.overflow = "hidden";
    else document.body.style.overflow = "auto";
  }, [visible]);

  function onClose() {
    if (!!document.body && !!document.body.style) {
      document.body.style.overflow = "auto";
    }
    onRequestClose();
  }

  return (
    <div className={style.dialogContainer} style={{ width: visible ? "100%" : "0", ...customStyle }}>
      {
        showCloseButton && <div onClick={() => onClose()}><FaTimesCircle color="#FFFFFF" size='1.5rem' /></div>
      }
      {children}
    </div>
  )
};

Dialog.propTypes = {
  onRequestClose: PropTypes.func.isRequired,
  visible: PropTypes.bool.isRequired,
  children: PropTypes.node.isRequired,
  customStyle: PropTypes.object,
  showCloseButton: PropTypes.bool,
};

Dialog.defaultProps = {
  customStyle: {
    backgroundColor: 'green',
  },
  showCloseButton: true,
};

export default Dialog;
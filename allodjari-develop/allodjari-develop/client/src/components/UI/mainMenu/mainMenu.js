import React from "react"
import { Link } from "gatsby";
import Dialog from "../dialog/dialog";
import style from './mainMenu.module.scss';
import PropTypes from "prop-types";
import * as auth from '../../../services/auth';

const MainMenu = ( props ) => {

    const isLoggedIn = auth.isLoggedIn();
    const {username} = auth.getCurrentUser();

    return (
        <Dialog {...props} customStyle={{top: 60}} showCloseButton={false}>
            <div className={style.mainMenuContainer}>

                {
                    isLoggedIn &&
                    <span>Connecté: {username}</span>
                }

                {
                    !isLoggedIn &&
                    <>
                        <Link to="/app/login/" className={style.menuLink}>Se connecter</Link>
                        <Link to="/app/register/" className={style.menuLink}>S'inscrire</Link>
                    </>
                }

                <Link to="/app/offers" className={style.menuLink}>Parcourir les jobs</Link>

                {
                    isLoggedIn &&
                    <span onClick={() => auth.logout()}>Déconnexion</span>
                }

            </div>
        </Dialog>
    )
};

MainMenu.propTypes = {
    onRequestClose: PropTypes.func.isRequired,
    visible: PropTypes.bool.isRequired,
};

export default MainMenu;

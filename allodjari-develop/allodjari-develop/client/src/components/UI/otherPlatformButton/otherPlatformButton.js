import React from "react"
import style from "./otherPlatformButton.module.scss";
import PropTypes from "prop-types";
import { FaGoogle, FaFacebookF } from "react-icons/fa";

const otherPlatformButton = ({ actionType, platform }) => {

  const returnButtonText = (actionType) => {
    let actionText, platformText;
    switch (actionType) {
      case 'login':
        actionText = 'Connexion';
        break;
      case 'register':
        actionText = 'Inscription';
        break;
      default:
        actionText = 'Missing action type';
        break;
    }
    switch (platform) {
      case 'facebook':
        platformText = 'Facebook';
        break;
      case 'google':
        platformText = 'Google';
        break;
      default:
        platformText = 'Missing platform';
        break;
    }
    return `${actionText} via ${platformText}`;
  }

  const returnActionIcon = (platform) => {
    switch (platform) {
      case 'google':
        return <FaGoogle className={[style.googleIcon, style.networkIcon].join(' ')} size='1.1rem' />;
      case 'facebook':
        return <FaFacebookF className={[style.facebookIcon, style.networkIcon].join(' ')} size='0.85rem' />;
      default:
        return <span>Missing platform</span>
    }
  }

  return (
    <a className={style.otherPlatformButton}>
      {returnActionIcon(platform)}
      <span>{returnButtonText(actionType, platform)}</span>
    </a>
  )
}


otherPlatformButton.propTypes = {
  actionType: PropTypes.oneOf(['login','register']).isRequired,
  platform: PropTypes.oneOf(['facebook', 'google']).isRequired,
}

export default otherPlatformButton
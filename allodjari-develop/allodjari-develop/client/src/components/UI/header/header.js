import React, { useState } from "react"
import { Link } from "gatsby"
import style from './header.module.scss'
import HeaderLogo from "./headerLogo";
import { FaBars, FaTimesCircle } from 'react-icons/fa';
import MainMenu from "../mainMenu/mainMenu";

const Header = () => {
  const [isMenuOpen, setIsMenuOpen] = useState(false);

  return (
    <>
      <header
        className={style.headerContainer}
      >
          <div className={style.headerLogoContainer}>
            <Link
              to="/"
            >
              <HeaderLogo />
            </Link>
          </div>

          <div onClick={() => setIsMenuOpen(!isMenuOpen)}>
            {
              isMenuOpen
                ? <FaTimesCircle color="#FFFFFF" size='1.5rem' />
                : <FaBars color='#FFFFFF' size='1.5rem' style={{margin: 'auto 0'}} />
            }
          </div>

      </header>
      <MainMenu visible={isMenuOpen} onRequestClose={() => setIsMenuOpen(!isMenuOpen)} />
    </>
  )
};

export default Header

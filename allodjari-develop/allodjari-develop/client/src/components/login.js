import React from "react";
import * as Yup from "yup";
import { Formik, Form, ErrorMessage, Field } from "formik";
import Layout from "components/UI/layout/layout";
import OtherPlatformButton from "components/UI/otherPlatformButton/otherPlatformButton";
import style from './login.module.scss';
import FormikFieldLabel from "./UI/form/formikFieldLabel/formikFieldLabel";
import { Link, navigate  } from "gatsby"
import * as auth from "../services/auth";

const Login = () => {

  if(auth.isLoggedIn()) {
    console.log("You are already connected :)");
    navigate(`/`)
  }

  Yup.setLocale({
    mixed: {
      required: 'Ce champ est requis',
    },
    string: {
      min: min => `${min.min} caractères minimum`,
      max: max => `${max.max} caractères maximum`,
      length: length => `${length.length} caractères requis`,
    },

  });

  const valuesSchema = Yup.object().shape({
    phoneNumber: Yup.string().required().length(10),
    password: Yup.string().required(),
  });

  const initialValues = {
    phoneNumber: '',
    password: ''
  };

  function handleSubmit(values, actions) {
    const { phoneNumber, password } = values;
    const payload = {
      username: phoneNumber,
      password: password
    };
    auth.handleUserLogin(payload)
        .then(_ => {
          navigate('/');
        })
        .catch();
    actions.resetForm();
  }

  return (
    <Layout backgroundColor="#DEDEDE">
      <h1 className={style.loginHeader}>Connexion</h1>

      <div className={style.loginFormCard}>
        <Formik initialValues={initialValues} validationSchema={valuesSchema} onSubmit={handleSubmit}>
          {({ values, setFieldValue }) => (
            <Form>
              <FormikFieldLabel fieldName='phoneNumber' label='Numéro de téléphone' fieldType='tel' placeholder='06 42 58 69 35' />
              <FormikFieldLabel fieldName='password' label='Mot de passe' fieldType='password' placeholder='********' />
              <button className={style.loginButton} type='submit'>Connexion</button>
            </Form>
            )}
        </Formik>
      </div>

      <div className={style.otherLinksContainer}>
        <Link to='/register' className={style.registerLink}>Pas encore inscrit ?</Link>
        <OtherPlatformButton platform='facebook' actionType='login' />
        <OtherPlatformButton platform='google' actionType='login' />
      </div>

    </Layout>
  );
};
export default Login;

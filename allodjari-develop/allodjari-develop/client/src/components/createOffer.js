import React, {Component} from "react";
import Layout from "components/UI/layout/layout";
import ChooseOfferCategory from "./createOfferComponents/chooseOfferCategory";
import OfferMainInfos from "./createOfferComponents/OfferMainInfos";
import CreateOfferHeader from "./createOfferComponents/createOfferHeader/createOfferHeader";
import OfferDetails from "./createOfferComponents/OfferDetails";
import OfferSummary from "./createOfferComponents/offerSummary";

const CreateOffer = () => {


    const [formStep, setFormStep] = React.useState(0);
    const [offerCategory, setOfferCategory] = React.useState("");
    const [offerTitle, setOfferTitle] = React.useState("");
    const [offerDescription, setOfferDescription] = React.useState("");
    const [offerCity, setOfferCity] = React.useState("");
    const [offerPictures, setOfferPictures] = React.useState("image");
    const values = {category: offerCategory, title: offerTitle, description: offerDescription, picture: offerPictures, city: offerCity, idUser: "/api/users/388"};

    const onSetCategory = (formikValues) => {
        !!formikValues && !!formikValues.category && setOfferCategory(formikValues.category);
        setFormStep(formStep+1);
    }

    const onSetMainInfos = (formikValues) => {
        !!formikValues && !!formikValues.title && setOfferTitle(formikValues.title);
        !!formikValues && !!formikValues.city && setOfferCity(formikValues.city);
        setFormStep(formStep+1);
    }

    const onSetDetails = (formikValues) => {
        !!formikValues && !!formikValues.description && setOfferDescription(formikValues.description);
        setFormStep(formStep+1);
    }

    const confirm = () => {
        console.log(values);
        fetch(`http://localhost:8000/api/offers`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(values)
        }).then(res => {
            console.log("confirm res:");
            console.log(res);
            return Promise.resolve(res);
        })
            .catch(err => {
                console.log("confirm err:");
                console.log(err);
                return Promise.reject(err);
            });

    }

    const getPages =  [
        {index:1, component: <ChooseOfferCategory onSubmit={onSetCategory} values={values} />},
        {index:2, component: <OfferMainInfos onSubmit={onSetMainInfos} values={values} />},
        {index:3, component: <OfferDetails onSubmit={onSetDetails} values={values} />},
        {index:4, component: <OfferSummary values={values} onConfirm={confirm} />}
    ];

    return (
        <Layout backgroundColor="#DEDEDE" displayHeader={false}>
            <CreateOfferHeader returnPath='/app/needService' stepTitle={'Étape ' + (formStep + 1) + '/4'}/>
            {
                getPages[formStep].component
            }
            <br/>
            Valeurs courantes:
            <span>{`Catégorie: ${offerCategory}`}</span>
            <span>{`Titre: ${offerTitle}`}</span>
            <span>{`Description: ${offerDescription}`}</span>
            <span>{`Ville: ${offerCity}`}</span>
            <span>{`Image: ${offerPictures}`}</span>
        </Layout>
    )

};

export default CreateOffer;
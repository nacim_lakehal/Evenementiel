import React from "react"
import { useStaticQuery, graphql } from "gatsby"
import Layout from "./UI/layout/layout";
import style from "./offersList.module.scss";
import { Link } from "gatsby";
import { FaMapMarkerAlt } from "react-icons/fa";

const OffersList = (props) => {

    const data = useStaticQuery(graphql`
        {
          api {
            offers {
              totalCount
              edges {
                node {
                  _id
                  city
                  title
                  category{
                    name
                  }
                }
              }
            }
          }
        }
     `);
    return   (
        <>
            <Layout backgroundColor='#DEDEDE'>
                <h1 className={style.ListOfferHeader}>Offres disponibles</h1>

                <div className={style.containerGlobal}>
                    {
                        data.api.offers.edges.map(offerNode => {
                            const {_id,title,city,category} = offerNode.node;
                            const offerUrl = "/app/offer/" + _id;
                            return (
                                <Link to={offerUrl} className={style.LinkOffer} id={_id}>
                                    <div className={style.containerOffer}>
                                        <h3 className={style.Nom}> {title} </h3>
                                        <h4 className={style.Nom}> {category.name} </h4>
                                        <div className={style.localisation}>
                                            <FaMapMarkerAlt color='#D61560' /><p className={style.pLocalisation}>A : <span className={style.ville}>{city}</span></p> </div>
                                    </div>
                                </Link>
                            )
                        })
                    }

                </div>
            </Layout>
        </>
    );
};

export default OffersList
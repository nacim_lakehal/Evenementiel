import React from "react"
import { useStaticQuery, graphql } from "gatsby"
import Layout from "./UI/layout/layout";
import style from "./prosList.module.scss";
import Image from "./image";
import { Link } from "gatsby";
import { FaMapMarkerAlt, FaAngleRight, FaCaretRight, FaCaretDown } from "react-icons/fa";

const ProsList = () => {

    const data = useStaticQuery(graphql`
        query {
          api {
            descriptionPros {
              edges {
                node {
                  _id
                  IdUser {
                    name
                    lastName
                  }
                  IdProfession {
                    label
                  }
                  feedBacks {
                    totalCount
                  }
                  ville
                }
              }
            }
          }
        }
   `);

    const [prosToDisplay, setProsToDisplay] = React.useState(data.api.descriptionPros.edges);
    const [filtersOpen, setFiltersOpen] = React.useState(false);

    return   (
        <>
            <Layout backgroundColor='#DEDEDE'>
                <h1 className={style.ListDescProHeader}>Annuaire pro</h1>
                <div className={style.filtersContainer}>
                    <div className={style.filtersHeader} onClick={() => setFiltersOpen(!filtersOpen)}>
                        <div className={style.filtersTitle}>Filtres</div>
                        {
                            filtersOpen ? <FaCaretDown /> : <FaCaretRight />
                        }
                    </div>
                    <div className={style.filtersList} style={filtersOpen ? {height: '100px'} : {}}>
                        Filtre 1<br/>
                        Filtre 2<br/>
                        Filtre 3<br/>
                        ... (todo)<br/>
                    </div>
                </div>
                {
                    prosToDisplay.map(descPro => {
                        const { _id, IdUser, feedBacks, ville, IdProfession } = descPro.node;
                        const { name, lastName } = IdUser;
                        const proProfileUrl = "/app/pro/" + _id;
                        const proName = name +' '+ lastName;
                        return (
                            <div className={style.containerPro} key={`proUser_`+_id}>
                                <Link to={proProfileUrl} className={style.LinkPro}>
                                    <div className={style.infosContainer}>
                                        <Image imageName={'avatar_x-large.png'} className={style.Avatar} id={_id}/>
                                        <div className={style.textInfos}>
                                            <div className={style.Nom}> {proName} </div>
                                            <div className={style.details}>{IdProfession.label}</div>
                                            <div className={style.localisation}>
                                                <FaMapMarkerAlt />
                                                <span>{ville}</span>
                                            </div>
                                        </div>
                                        <div className={style.goToIcon}>
                                            <FaAngleRight size='2.3rem' />
                                        </div>
                                    </div>
                                </Link>
                            </div>
                        )
                    })
                }
            </Layout>
        </>
    );
};

export default ProsList

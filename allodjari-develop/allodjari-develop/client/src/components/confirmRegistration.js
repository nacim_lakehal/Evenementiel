import React from "react";
import Layout from "./UI/layout/layout";
import pageStyles from "../pages/pageStyles.module.scss";

const ConfirmRegistrationPage = () => {
  return (
    <Layout backgroundColor="#DEDEDE">
      <h1 className={pageStyles.pageHeaderTitle}>Inscription</h1>
    </Layout>
  )
};

export default ConfirmRegistrationPage;
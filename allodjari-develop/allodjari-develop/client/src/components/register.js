import React from "react";
import { Form, Formik } from "formik";
import * as Yup from "yup";
import Layout from "./UI/layout/layout";
import OtherPlatformButton from './UI/otherPlatformButton/otherPlatformButton';
import FormikFieldLabel from "./UI/form/formikFieldLabel/formikFieldLabel";
import FormikCheckbox from "./UI/form/formikCheckbox/formikCheckbox";
import style from './register.module.scss';
import { handleUserLogin } from '../services/auth';
import * as auth from "../services/auth";
import {navigate} from "gatsby";

const Register = () => {

  if(auth.isLoggedIn()) {
    console.log("You are connected :)");
    navigate(`/`)
  }

  Yup.setLocale({
    mixed: {
      required: 'Ce champ est requis',
    },
    string: {
      min: min => `${min.min} caractères minimum`,
      max: max => `${max.max} caractères maximum`,
      length: length => `${length.length} caractères requis`,
    },

  });

  const valuesSchema = Yup.object().shape({
    name: Yup.string().required(),
    lastName: Yup.string().required(),
    phoneNumber: Yup.string().required().length(10),
    password: Yup.string().required(),
    passwordMatch: Yup.string().required().test('passwords-match', 'Les mots de passe doivent être identiques', function(value) {
      return this.parent.password === value;
    }),
    cgu: Yup.boolean().required().oneOf([true], 'Vous devez accepter les CGU pour utiliser la plateforme AlloDjari.'),
  });

  const initialValues = {
    phoneNumber: '',
    password: '',
    passwordMatch: '',
    cgu: false,
    name: '',
    lastName: '',
  };

  function handleSubmit(values, actions) {
    console.log("handle submit");
    const { phoneNumber, password } = values;
    const payload = {
      username: phoneNumber,
      password: password
    };
    handleUserLogin(payload);
    actions.resetForm();
    console.log(payload);
    console.log(actions);
  }

  return (
    <Layout backgroundColor="#DEDEDE">
      <h1 className={style.registerHeader}>Inscription</h1>
      <div className={style.textInfoContainer}>
        <span>Vous souhaitez demander ou proposer vos services ?</span><br/>
        <b>Inscrivez-vous sur AlloDjari et entrez en contact avec plus de XX citoyens et professionnels algériens.</b>
      </div>
      <div className={style.otherLinksContainer}>
        <OtherPlatformButton platform='facebook' actionType='register' />
        <OtherPlatformButton platform='google' actionType='register' />
      </div>
      <div className={style.registerAlloDjari}>
        <h2>Inscription sur AlloDjari</h2>
        <Formik initialValues={initialValues} validationSchema={valuesSchema} onSubmit={handleSubmit}>
          {({ values, setFieldValue }) => (
            <Form className={style.alloDjariForm}>
              <FormikFieldLabel fieldName='name' label='Prénom' fieldType='text' placeholder='Saad' />
              <FormikFieldLabel fieldName='lastName' label='Nom' fieldType='text' placeholder='Lamarmotte' />
              <FormikFieldLabel fieldName='phoneNumber' label='Numéro de téléphone' fieldType='tel' placeholder='06 42 58 69 35' />
              <FormikFieldLabel fieldName='password' label='Mot de passe' fieldType='password' placeholder='********' />
              <FormikFieldLabel fieldName='passwordMatch' label='Confirmez le mot de passe' fieldType='password' placeholder='********' />
              <FormikCheckbox label="J'accepte les Conditions Générales" fieldName='cgu' />
              <button className={style.registerButton} type='submit'>S'inscrire</button>
            </Form>
          )}
        </Formik>
      </div>
    </Layout>
  );
};

export default Register;
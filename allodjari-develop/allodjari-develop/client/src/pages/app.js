import React from "react"
import { Router } from "@reach/router"
import Layout from "../components/UI/layout/layout";
import Login from "../components/login";
import Register from "../components/register";
import PrivateRoute from "../components/UI/privateRoute";
import NeedService from "../components/needService";
import ProsList from "../components/prosList"
import CreateOffer from "../components/createOffer";
import OffersList from "../components/offersList"

const App = () => (
  <Layout displayHeader={false} backgroundColor='transparent'>
    <Router>
      <Login path="/app/login" />
      <Register path="/app/register" />
      <NeedService path="/app/needService" />
      <PrivateRoute path="/app/createOffer" component={CreateOffer} />
      <ProsList path="/app/pros"/>
      <OffersList path="/app/offers"/>
    </Router>
  </Layout>
);

export default App;
import React from "react"
import { Link } from "gatsby"
import { useTranslation } from 'react-i18next';
import 'styles/fonts.scss';
import Layout from "../components/UI/layout/layout";
import '../i18n';
import style from "./index.module.scss";



const IndexPage = ({ data }) => {
    const { t } = useTranslation('UI');

    const renderDescPros = () => {
        return (
            <div>
                {
                    data.api.descriptionPros.edges.map(descNode =>{
                        const {id, IdUser, Description} = descNode.node;
                        const {name, lastName } = IdUser;
                        return (
                            <div key={id}>
                                {name} {lastName}<br/>
                            </div>
                        )
                    })
                }
            </div>
        )
    }

    return (
        (
            <Layout backgroundColor='#DEDEDE'>
                <div className={style.backgroundContainer}>
                    <div className={style.container}>
                        <div className={style.callTexts}>
                            <div className={style.titrePrincipale}>Besoin d'un coup de main<br/>pour un projet ?</div>
                            <div className={style.titreBas}>
                                Trouvez un professionnel<br/>compétent sur
                                <div className={style.AlloDjari}>AlloDjari</div>
                            </div>

                        </div>
                        <div className={style.buttonLinksContainer}>
                            <Link to={'/app/needService'} className={style.needServiceButton}>J'ai besoin d'un service</Link>
                            <Link to={'/app/pros'} className={style.findProButton}>Trouver un professionnel</Link>
                        </div>
                    </div>
                </div>
            </Layout>
        )
    )
};

export default IndexPage;

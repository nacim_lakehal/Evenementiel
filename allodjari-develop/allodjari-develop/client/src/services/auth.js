import { navigate  } from "gatsby"

const isBrowser = typeof window !== `undefined`;

const getUser = () => window.localStorage.gatsbyUser
    ? JSON.parse(window.localStorage.gatsbyUser)
    : {};

const setUser = user => (window.localStorage.gatsbyUser = JSON.stringify(user));

export const isLoggedIn = () => {
    if (!isBrowser) return false;
    const user = getUser();
    return !!user.token
}

export function XHTTP(route, params = {}){
    return new Promise((resolve, reject) => {
        const url = `${route}`;
        fetch(url, params)
            .then((response) => {
                const call = response.status < 400 ? resolve : reject;
                const ct = response.headers.get("Content-Type");
                if (response.status === 204) {
                    call();
                } else if (ct !== null && (ct.includes("text/plain") || ct.includes("text/csv"))) {
                    response
                        .text()
                        .then(call)
                        .catch(() => call(response));
                } else {
                    response
                        .json()
                        .then(call)
                        .catch(() => call(response));
                }
            })
            .catch(error => {
                if (error && error.json) return error.json().then(reject);
                reject();
            });
    });
}

function login(username, password) {
    return XHTTP(`http://localhost:8000/api/login_check`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            username,
            password
        })
    });
}

export const handleUserLogin = ({ username, password }) => {
    return login(username, password)
        .then(res => {
            const { token } = res;
            setUser({
                token,
                username
            });
            console.log('User login succeeded.');
            return Promise.resolve(res);
        })
        .catch(err => {
            console.log("User login error:");
            console.log(err);
            return Promise.reject(err);
        });
}

export const getCurrentUser = () => isBrowser && getUser();

export const logout = (callback = () => navigate(`/`)) => {
    if (!isBrowser) return
    console.log(`Logging out... goodbye!`)
    setUser({});
    callback();
}

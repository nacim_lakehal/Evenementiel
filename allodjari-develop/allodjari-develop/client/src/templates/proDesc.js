import React from "react"
import { graphql, Link } from "gatsby"
import Layout from "../components/UI/layout/layout";
import styles from "./proDesc.module.scss";
import { FaCalendarDay, FaChevronLeft, FaInfoCircle, FaMapMarkerAlt, FaQuoteLeft } from 'react-icons/fa';
import moment from 'moment';


const ProDesc = ({ data }) => {

    const descPro = data.api.descriptionPro;

    const { IdUser, Description, IdProfession, ville, createdAt, codPostal, NbMission, feedBacks } = descPro;

    const { name, lastName } = IdUser;
    const labelMetier = IdProfession.label;
    const nbFeedbacks = feedBacks.totalCount;
    const createDate = moment(Date.parse(createdAt)).format("DD/MM/YYYY");
    const anciennete = moment(Date.now()).diff(Date.parse(createdAt), 'days');

    return (
        <Layout backgroundColor="#D61560">
            <h1 className={styles.pageHeader}>Profil pro</h1>
            <Link to="/app/pros" className={styles.returnLink}><FaChevronLeft size='1rem' />Retour</Link>

            <div className={styles.descProContainer}>
                <div className={styles.descProHeader}>
                    <div className={styles.proProfilePicture}>

                    </div>
                    <div className={styles.descProTitle}>
                        <div className={styles.proName}>
                            {`${name} ${lastName}`}
                        </div>
                        <div className={styles.profession}>
                            {`${labelMetier}`}
                        </div>
                    </div>
                </div>
                <div className={styles.descProContent}>
                    <div className={styles.descProItem}>
                        <FaInfoCircle size='1.5rem' />
                        <span>{Description}</span>
                    </div>
                    <div className={styles.descProItem}>
                        <FaCalendarDay size='1.5rem' />
                        <span>Inscrit il y a {anciennete} jours</span>
                    </div>
                    <div className={styles.descProItem}>
                        <FaMapMarkerAlt size='1.5rem' />
                        <span>{ville} ({codPostal})</span>
                    </div>
                    <div className={styles.descProItem}>
                        <FaQuoteLeft size='1.5rem' />
                        <span>Évaluations ({nbFeedbacks})</span>
                    </div>
                </div>

            </div>
        </Layout>
    )

};

export default ProDesc;

export const query = graphql`
  query GetUser($proId: ID!) {
      api {
        descriptionPro(id: $proId) {
          Description
          IdProfession {
            label
          }
          IdUser {
            lastName
            name
          }
          ville
          createdAt
          datDebut
          NbMission
          codPostal
          feedBacks {
            totalCount
          }
        }
      }
    }
`;
import React from "react"
import { graphql, Link } from "gatsby"
import Layout from "../components/UI/layout/layout";
import styles from "./offerDesc.module.scss";
import { FaChevronLeft, FaMapMarkerAlt } from 'react-icons/fa';
import moment from 'moment';
import Image from "../components/image";


const OfferDesc = ({ data }) => {

    const descOffer = data.api.offer;

    const { city, createdAt, description, title, beginDate, idUser } = descOffer;
    const userName = idUser.name;

    const beginDateText = moment(Date.parse(beginDate)).format("DD/MM/YYYY");
    const createdAtText = moment(Date.parse(createdAt)).format("DD/MM/YYYY");
    const ancienneteOffre = moment(Date.now()).diff(Date.parse(createdAt), 'days');

    return (
        <Layout backgroundColor="#D6C515">
            <h1 className={styles.pageHeader}>Appel d'offre</h1>
            <Link to="/app/offers" className={styles.returnLink}><FaChevronLeft size='1rem' />Retour</Link>

            <div className={styles.descOfferContainer}>
                <div className={styles.descOfferHeader}>
                    Job proposé par
                    <div className={styles.userProfile}>
                        <Image imageName={'avatar_x-large.png'} className={styles.userAvatar}/>
                        <span>{userName}</span> il y a {ancienneteOffre} jour(s)
                    </div>
                </div>
                <div className={styles.descOfferContent}>
                    <div className={styles.descOfferItem}>
                        <span>{title}</span>
                    </div>
                    <div className={styles.descOfferItem}>
                        {description}
                    </div>
                    <div className={styles.descOfferItem}>
                        <FaMapMarkerAlt size='1.5rem' />
                        {city}
                    </div>
                </div>
                <div className={styles.callButton}>Proposer mes services</div>

            </div>
        </Layout>
    )

};

export default OfferDesc;

export const query = graphql`
    query getOffer($offerId: ID!) {
      api {
        offer(id: $offerId) {
          city
          createdAt
          description
          title
          beginDate
          idUser {
            name
          }
        }
      }
    }
`;
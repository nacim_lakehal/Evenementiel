import i18n from "i18next"
import LanguageDetector from "i18next-browser-languagedetector";
import { initReactI18next } from "react-i18next";
import * as UI from './locales/fr/UI';

const resources = {
  fr: [UI]
};

i18n
  .use(LanguageDetector)
  .use(initReactI18next)
  .init({
    lng: 'fr',
    fallbackLng: 'fr',
    ns: ["UI"],
    defaultNS: "UI",

    debug: true,
    interpolation: {
      escapeValue: false, // not needed for react
    },
    react: {
      wait: true,
      useSuspense: false,
    },
  })
  .then(value => console.log(value))
  .catch(error => console.log(error));

export default i18n;
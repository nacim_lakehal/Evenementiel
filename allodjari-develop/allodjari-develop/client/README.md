## Front-end AlloDjari

### À propos

Le front est construit avec Gatsby.js [(docs)](https://www.gatsbyjs.org/docs/).  
Les maquettes interactives sont disponibles  [ici](https://xd.adobe.com/view/af5f2dc7-115f-4a99-68d1-87ed1b35b1a4-d96e/grid).

### Structure du projet
* `/client`: contient la partie front-end faite en React avec Gatsby
    * `/src`: fichiers sources
        * `/components`: composants React
        * `/pages`: vues/pages du site
        * `/assets`: assets statiques du site (fonts, images)
        * `/styles`: styles communs à tout le site
        * `/locales`: fichiers de traduction
    
### Lancer le projet
1. Cloner le dépôt
2. `cd allodjari && cd client`
3. `npm install`
    * installation des dépendances, à ne faire que lors de la première installation ou lorsqu'elles changent
4. `npm start`
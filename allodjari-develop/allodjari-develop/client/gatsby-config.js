module.exports = {
  siteMetadata: {
    //set here every global site data
    title: `AlloDjari`,
    description: `AlloDjari: la mise en relation entre particuliers et professionnels simplifiée.`,
    author: `AlloDjari`,
  },
  proxy: {
    prefix: "/api",
    url: "http://localhost:8000",
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/assets/images`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `allodjari`,
        short_name: `allodjari`,
        start_url: `/`,
        background_color: `#0E738A`,
        theme_color: `#0E738A`,
        display: `minimal-ui`,
        icon: `src/assets/images/icon_no-bg.png`, // This path is relative to the root of the site.
      },
    },
    `gatsby-plugin-offline`, // To learn more, visit: https://gatsby.dev/offline
    {
      resolve: `gatsby-plugin-sass`,
      options: {
        implementation: require("sass"), //use dart-sass implementation instead of libsass via the node-sass lib
        includePaths: [`${__dirname}/src/styles`]
      }
    },
    {
      resolve: "gatsby-source-graphql",
      options: {
        // This type will contain remote schema Query type
        typeName: "API",
        // This is the field under which it's accessible
        fieldName: "api",
        // URL to query from
        url: "http://localhost:8000/api/graphql",
      },
    },
  ],
};

/**
 * Implement Gatsby's Browser APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/browser-apis/
 */

import "./src/styles/_base.scss";
import "./src/pages/pageStyles.module.scss";
import "./src/components/createOfferComponents/createOfferStyles.module.scss";
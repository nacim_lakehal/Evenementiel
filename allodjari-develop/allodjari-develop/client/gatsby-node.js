/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/node-apis/
 */
const fs = require("fs-extra");
const path = require("path");

// copy locales files into right folder for build
exports.onPostBuild = () => {
  console.log("Copying locales");
  fs.copySync(
    path.join(__dirname, "/src/locales"),
    path.join(__dirname, "/public/locales")
  )
};

// allows absolute imports
exports.onCreateWebpackConfig = ({ actions }) => {
  actions.setWebpackConfig({
    resolve: {
      modules: [path.resolve(__dirname, "src"), "node_modules"],
    },
  })
};

// serves routes for auth process
exports.onCreatePage = async ({ page, actions }) => {
  const { createPage } = actions
  // page.matchPath is a special key that's used for matching pages
  // only on the client.
  if (page.path.match(/^\/app/)) {
    page.matchPath = "/app/*"
    // Update the page.
    createPage(page)
  }
};


exports.createPages = async function ({ actions, graphql }) {
  const { data } = await graphql(`
    query {
      api {
        descriptionPros(first: 1000) {
          edges {
            node {
              id
              _id
            }
          }
        }
        offers(first: 100) {
          edges {
            node {
              _id
              id
            }
          }
        }
      }
    }
  `);
  if(!!data.api.descriptionPros) {
    console.log("Building " + data.api.descriptionPros.edges.length + " pro pages...");
    data.api.descriptionPros.edges.forEach(edge => {
      const desc = edge.node;
      const id = desc._id;
      const idText = desc.id;
      actions.createPage({
        path: '/app/pro/' + id,
        component: require.resolve(`./src/templates/proDesc.js`),
        context: {
          proId: idText
        },
      })
    });
  }

  if(!!data.api.offers) {
    console.log("Building " + data.api.offers.edges.length + " offers pages...");
    data.api.offers.edges.forEach(edge => {
      const desc = edge.node;
      const id = desc._id;
      const idText = desc.id;
      actions.createPage({
        path: '/app/offer/' + id,
        component: require.resolve(`./src/templates/offerDesc.js`),
        context: {
          offerId: idText,
        },
      })
    });
  }

};